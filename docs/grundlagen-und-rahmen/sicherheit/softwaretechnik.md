---
sidebar_position: 2
description: Welche technischen Aspekte unterstützen IT-Sicherheit?
---

# Softwaretechnik-Aspekte

Gesetzliche Anforderungen und Standards beziehen sich eher auf offensichtlich sicherheitsrelevante Teile eines Softwaresystems, wie z.B. Identitätsmanagement oder Verschlüsselungsverfahren [@McGrawBuildingSecurity]. Doch auch in Spezifikation, Architektur und in der Implementierung unkritischer Anwendungslogik können wichtige Weichen gestellt werden, um ein insgesamt sichereres Produkt zu erhalten [@McGrawBuildingSecurity]. Die folgenden Abschnitte beleuchten Berührungspunkte zwischen Prozessen aus der Softwaretechnik und Sicherheit.

## Sicherheitsanforderungen

Die Sicherheit einer Software ist keine intrinsische Eigenschaft. Zunächst sollte klar sein, was genau gesichert werden soll. Diese schützenswerten "Assets" sind häufig Daten bzw. Informationen, aber auch Infrastruktur und je nach Einsatzbereich auch Personen. Üblich sind die folgenden Aspekte, um Sicherheitsziele bzw. konkrete Sicherheitsregeln ("Policies") abzuleiten [@McGrawBuildingSecurity]:
- Vertraulichkeit. Bestimmte Informationen sollen z.B. nur bestimmten Personenkreisen zugänglich sein/bleiben.
- Integrität. Bestimmte Informationen sollen unverändert sein/bleiben.
- Verfügbarkeit. Bestimmte Informationen sollen verfügbar sein/bleiben.

Weiterhin sollte auch klar sein, wovor genau die Assets geschützt werden sollen. Dazu dient der Abschnitt [Bedrohungsmodellierung](#bedrohungsmodellierung---threat-modeling). Bedrohungen stellen je nach Auswirkung und Wahrscheinlichkeit ein **Risiko** dar. Wir behandeln den Umgang damit in den Abschnitten zu [Risikomanagement](#risikomanagement) und zu [Sicherheitskontrollen](#sicherheitskontrollen), aus denen wir wiederum Anforderungen für unser Softwareprojekt ableiten.

## Sicherheit im Design

Das [OWASP](https://owasp.org) führte 2021 eine neue Kategorie von Schwachstelle ein: unsicheres Design [@OWASPA042021]. Denn im Requirements Engineering und in der Software- bzw. Systemarchitektur können genauso Schwachstellen entstehen wie in der Implementierung. Diese sind im Zweifelsfall deutlich schwerer zu beheben. Eine Implementierung eines unsicheren Designs kann fehlerfrei sein und bleibt dennoch unsicher. Im Folgenden gehen wir auf Designaspekte ein, die in jedem Fall berücksichtigt werden sollten.

## Risikomanagement mit Sicherheitskontrollen

Sicherheitskontrollen sind zusätzliche Anforderungen an die Software, die bestimmte Risiken aus erwarteten Bedrohungen reduzieren. Es gibt unterschiedliche Arten, die sich vor allem in Aufwand und Effektivität unterscheiden. Nicht immer ist es sinnvoll, das Risiko vollständig vermeiden zu wollen. Folgende Aufgaben können Sicherheitskontrollen übernehmen [@MicrosoftOpSecPractice]:
- Prävention. Erschwert Eintritt von Bedrohungen, z.B. eine Passwortabfrage.
- Detektion. Identifiziert Eintritt von Bedrohungen, z.B. ein Zugriffslog.
- Korrektur. Reagiert auf möglichen Eintritt von Bedrohungen, z.B. automatische Updates.
- Wiederherstellung. Behebt Schaden nach Eintritt von Bedrohung, z.B. Backup.
- Abschreckung. Baut Abstand zu Bedrohungen auf, z.B. niedrigste Privilegien.

Sicherheitskontrollen müssen auch nicht zwangsläufig in der Software realisiert werden. Auch die Organisation kann ihren Beitrag leisten, etwa durch IT-Notfallpläne und Richtlinien. Teile dieser Kontrollen werden auch von Infrastruktur wie z.B. Cloudplattformen umgesetzt. Weiterhin kann die physikalische Komponente eine wichtige Rolle spielen, etwa mit Kameraüberwachung und Türschlössern.

Hier einige Vorschläge für präventive Sicherheitskontrollen, die in Software auf architektureller Ebene umgesetzt werden können, um typischen Bedrohungen (nach STRIDE, siehe [@MicrosoftSDL]) zu begegnen.

| Bedrohung              | Präventive Sicherheitskontrolle                          |
|------------------------|----------------------------------------------------------|
| Spoofing               | Authentisierung: Benutzername/Passwort, Zertifikat, TOTP |
| Tampering              | Message Authentication Codes, digitale Signaturen        |
| Repudation             | Zugriffslog, digitale Signaturen                         |
| Information Disclosure | Verschlüsselung                                          |
| Denial of Service      | Limitierte Zugriffsrate                                  |
| Elevation of Privilege | Niedrigste Privilegien konsequent umsetzen               |

Präventive Sicherheitskontrollen sind nicht immer das Mittel der Wahl. Sie sind häufig komplexer und teurer als Alternativen und erscheinen gelegentlich mit sekundären Risiken. Beispielsweise ist es nahezu unmöglich, einen Ransomware-Befall kategorisch zu verhindern. Gleichzeitig ist hier ein Backup als Sicherheitskontrolle effektiv. Es verringert die Folgeschäden erheblich, obwohl es einen Befall nicht verhindert.

## Quellen
