---
sidebar_position: 3
description: kurze Einführung, Problematisirung und Auflistung der wichtigsten Akteure in der Verwaltungsdigitalisierung
---

# Akteure

Die Akteurslandschaft im Kontext von Verwaltungsdigitalisierung und [OZG](docs/grundlagen-und-rahmen/ozg) (Onlinezugangsgesetz) ist sehr heterogen. Neben politischen Gremien (z.B. IT-Planungsrat), Ministerien auf  europäischer, Bundes-, Landes- und kommunaler Ebene und einzelnen Digitalbeauftragten sind auch teilweise unabhängige Forschungs- und Innovationseinrichtungen am Projekt digitale Verwaltung beteiligt. Zudem wirken auch private IT-Dienstleister und Beratungsunternehmen aktiv mit.

Die teilweise nicht-statischen Gegebenheiten machen eine umfassende Beschreibung der an Digitalisierung von Verwaltung beteiligten Akteuren nahezu unmöglich, weshalb auf den folgenden Seiten eine Auswahl relevanter Akteure und deren Rolle nach Wirkungsbereich gegliedert und je nach Gewichtung mehr oder weniger intensiv diskutiert oder gar nur mit weiterführenden Links versehen werden. Zudem kann diese Auflistung keinen Anspruch auf Vollständigkeit erheben. Abschließend folgt ein kurzer Abschnitt dazu, welche Steuerungsproblematiken in der dargestellten Akteurslandschaft zu beachten sind.

## Allgemeines zur Akteurslandschaft

Viele Digitalisierungsprojekte sind ebenenübergreifend angelegt, weshalb es oftmals schwierig ist einen Überblick über die involvierten Akteure zu gewinnen. Der Grund für eine solche Vielzahl an Akteuren ist zum einen die deutsche Staatsorganisation und die Weise, wie der Vollzug von Verwaltungsleistungen geregelt ist. Zudem resultiert die Anzahl aus der Art von Projekten, in denen Technik, Recht und Organisation zusammengedacht werden müssen. Zu einiger Berühmtheit in der deutschen Digitalisierungscommunity hat es das Wimmelbild zu den Strukturen der OZG-Umsetzung gebracht, das der [Nationale Normenkontrollrat](https://www.normenkontrollrat.bund.de/Webs/NKR/DE/home/home_node.html) (NKR) mit einem etwas ironischen Kommentar veröffentlicht hat. Damit wird grundsätzlich bezweifelt, dass solche Strukturen überhaupt für die Umsetzung geeignet sind.

![Wimmelbild des Normenkontrollrats](https://www.oeffentliche-it.de/documents/10181/181462/abbildung1_large.jpg)
[@NKR_Bericht]

Im Grundsatz zeigt das Wimmelbild aber auf, dass es sich um eine sehr heterogene Akteurslandschaft handelt, die aus vielen unterschiedlichen Stellen, Ministerien und Gremien auf Bundes- und Landesebene sowie kommunaler Ebene besteht. Am Beispiel der Länderebene lässt sich das beispielhaft zeigen: Während das eine Land CIO’s (Chief Information Officer, z.B. Hessen) oder auch CDO’s (Chief Digital Officer) einsetzt, deren Aufgaben und Zuständigkeiten wiederum von Land zu Land unterschiedlich definiert sind, legen andere die Verantwortung ihrer Digitalisierungsvorhaben in die Hände so genannter Digitalkabinette oder „X-Y-Digitalministerien“ [@Berzel_Länder]. Manchmal existieren diese Institutionen auch nebeneinander.

Betrachtet man die Digitalisierungslandschaft und ihre Entwicklung als eine Art wechselwirkendes Netz, lassen sich fluide Akteurskonstellationen definieren. Diese resultieren u.a. daraus, dass, wie im Kapitel zum [EGovG](docs/grundlagen-und-rahmen/e-government) beschrieben, die in Gesellschaft und Wirtschaft fortschreitende technische Entwicklung und dadurch induzierte Effizienzsteigerung Bestrebungen für eine Verwaltungsmodernisierung und -digitalisierung auslöst, insbesondere auch der Wunsch von Bürger:innen und Unternehmen nach einem nutzerfreundlicheren Zugang zu Verwaltungsleistungen [@NKR_Bericht2021] [@Martini_Transformation]. 

## Bereich Recht, Verordnung, Weisung und Evaluation

### EU-Kommission
Im Kontext digitaler Verwaltung ist die Europäische Kommission die zentrale Akteurin. Sie ist verantwortlich für die Entwicklung einer umfassenden Digitalisierungsstrategie der EU hinsichtlich Standards, digitaler Souveränität, einer effizienten IT-Infrastruktur und ebenso Daten- und Cybersicherheit betreffend. Entsprechend des Europäischen Gedankens soll dadurch eine internationale Vernetzung der Mitgliedstaaten, genauer ihrer Digitalisierungsvorhaben, gefördert werden. Unter dem Stichwort barrierefreie und grenzüberschreitende Dienste soll somit ein ungehinderter Handel und Mobilität für die Bürgerinnen und Bürger gewährleistet werden. In Zusammenarbeit mit dem EU-Parlament und dem Europäischen Rat, bestehend aus Minister:innen der EU-Mitgliedsstaaten, können die erarbeiteten Strategien sodann durch rechtliche Beschlüsse und der Regelung des Haushalts unterstützt werden. Zu berücksichtigen ist jedoch, dass die EU keine Regelungen in Bezug auf die Verwaltungsorganisation in den Mitgliedstaaten treffen darf. 
Das Thema Informations- und Kommunikationstechnologie im Allgemeinen versucht die Europäische Kommission seit ihrem Bestehen aktiv zu begleiten und mitzugestalten. Aus diesem Anspruch wurde 2010 die digitale Agenda „Europa 2020“ in Form eines 10-Jahresplans beschlossen [@Wewer_EC]. Diese umfasst ein breites Feld, auf welchem Europa in Sachen Digitalisierung agieren möchte. Die Schwerpunkte sind dabei folgendermaßen gesetzt:

* „Schaffung eines digitalen Binnenmarkts,
* Verbesserung der Interoperabilität, Normen und Standards,
* Stärkung des Vertrauens und der Online-Sicherheit,
* Förderung eines schnellen und besonders schnellen Internetzugangs,
* Steigerung der Investitionen in Forschung und Innovation,
* Verbesserung der digitalen Kompetenzen, der Qualifikationen und der Integration in den Arbeitsmarkt“ [@Wewer_EC].

Dadurch sollen besonders die unterschiedlichen Entwicklungsstände hinsichtlich Digitalisierung zwischen den Mitgliedstaaten angeglichen werden, wobei der Fokus in der Förderung der Nachzügler liegt. Zudem soll der digitale Ausbau den gemeinsamen Europäischen Markt stärken und grenzüberschreitende elektronische Transaktionen ermöglichen. Dies schließt auch einen weltweit wettbewerbsfähigen europäischen Digitalmarkt mit ein. Und zuletzt möchte Europa die Führung als global führender Innovationsraum für IK-Technologie einnehmen [@Wewer_EC].
Mit dem Einsetzen des Vizepräsidenten für den digitalen Binnenmarkt (2014), der darauffolgenden entwickelten Strategie für diesen (2016) sowie die 2016 formulierte Agenda für die kollaborative Wirtschaft wird der Schwerpunkt zusätzlich forciert [@Wewer_EC]. Hinsichtlich Digitalisierungsbemühungen des öffentlichen Sektors und somit deren eigenen Arbeit nutzt die Europäische Kommission zwar früh den eigenen Webauftritt für Informationen und „Online-Konsultationen“, kann jedoch nur vereinzelt konkrete Impulse in Form vom Richtlinien oder Agenden setzen [@Wewer_EC]. Erst mit dem eGovernment-Aktionsplans fördert sie aktiv „die Transformation des öffentlichen Sektors innerhalb der Union“, auch mit der [Single-Digital-Gateway-Verordnung](docs/grundlagen-und-rahmen/sdg) (SGD-VO). Nicht zuletzt setzt sie sich auch aktiv mit den Themen Open Government und Open Data auseinander [@Wewer_EC].

Der Europäischen Kommission ist demnach die Rolle der Anstoßgeberin und Antreiberin zuzuschreiben, welche die Mitgliedstaaten vermittels Richtlinien, Verordnungen, Förderprogrammen oder Leitlinien aktivieren möchte [@Wewer_EC]. Angesichts des von Wewer beschriebenen Aktivitätsgrad durch den Beschluss von Richtlinien, wie der NIS-Richtlinie (Netzwerk- und Informationssicherheit), den Ausbau der Europäischen Agentur für Netz- und Informationssicherheit (ENISA, 2018), oder verschiedenster Evaluationen des digitalen Fortschritts in Europa (DAS, DESI, EDPR, IDESI), scheint die Europäische Kommission ihre Rolle ernst zu nehmen [@Wewer_EC]. Letztere zeigen jedoch, dass die nationalen Fortschritte stark voneinander abweichen, unabhängig von einzelnen Bereichen und dem bisherigen Entwicklungsstand. Der IDESI, International Digital Economy and Society Index verrät zudem, dass die Mitgliedstaaten im internationalen Vergleich besonders im Bereich Public Sector weit zurück liegen [@Wewer_EC]. Entsprechend sollte kritisch auf die bisherigen Bemühungen geblickt werden und die Frage nach einer „schnellere[n] digitale[n] Transformation von Wirtschaft und Verwaltung“ gestellt werden [@Wewer_EC].

### Bundeskanzler/CDBKA
Der Chef des Bundeskanzleramts nimmt eine Koordinierende Stellung zwischen Regierungsoberhaupt, den Ministerien und den Vertreter:innen der Länder ([Bundesregierung: CDBKA](https://www.bundesregierung.de/breg-de/bundesregierung/bundeskanzleramt/bundeskabinett/wolfgang-schmidt-1974220)).


### Digitalkabinett (Digitalrat)
Das Digitalkabinett besteht aus Bundeskanzler:in, den Bundesminister:innen sowie der/den Staatsminister:inen und bildet das Steuerungsgremium des Bundes in allen Fragen und Belangen zu Digitalisierung. Es besteht seit 2018 und dient dem ressortübergreifenden Austausch auf höchster politischer Ebene. Unter anderem wurde in diesem Rahmen die Umsetzungsstrategie für eine digitale Zukunft des Landes beschlossen ([Bundesregierung](https://www.bundesregierung.de/breg-de/suche/digitalkabinett-1763446)).


### BMI (als Stellvertreterin für die Bundesregierung)
Das Bundesministerium für Inneres, Heimat und Bau versteht sich im Kontext von Staats- und Verwaltungsmodernisierung als „Verfassungs- und Kommunalministerium“ und hat somit im Namen der Bundesregierung die Verantwortung für diesen Bereich inne. Sie beherbergt u.a. Abteilung DV (digitale Verwaltung) und setzt den Beauftragten der Bundesregierung für Informationstechnik ein ([BMI.de](https://www.bmi.bund.de/DE/ministerium/das-bmi/das-bmi-node.html), [Abteilung DV](https://www.bmi.bund.de/DE/ministerium/das-bmi/abteilungen-und-aufgaben/abteilungen-und-aufgaben-node.html#doc9390452bodyText11)).

 
### Beauftragte der Bundesregierung für Informationstechnik (Bundes-CIO)
Dieser fungiert als zentraler Ansprechpartner für Länder und Wirtschaft in IT-Fragen und verantwortet den Ausbau einer ressortübergreifende IT-Koordinierung zu einer ressortübergreifenden IT-Steuerung. Gemeinsam mit dem IT-Rat und der IT-Beauftragtenkonferenz treibt er diese Ziele voran ([BMI:Beauftragter der Bundesregierung für Informationstechnik](https://www.bmi.bund.de/DE/ministerium/beauftragte/beauftragter-informationstechnik/beauftragter-informationstechnik-artikel.html)).

 
### IT-Rat
Der IT-Rat des Bundes besteht aus den „für Verwaltungsdigitalisierung und Informationstechnik zuständigen beamteten Staatssekretärinnen und Staatssekretäre aller Bundesministerien“ und ist für „für die übergreifende strategische Steuerung der IT des Bundes einschließlich der strategisch relevanten Themen der IT-Konsolidierung des Bundes“ verantwortlich. Der IT-Rat ist demnach verantwortlich für die erfolgreiche und fristgerechte Umsetzung aller Projekte im Kontext der Verwaltungsdigitalisierung ([CIO Bund: IT-Rat](https://www.cio.bund.de/Webs/CIO/DE/cio-bund/steuerung-it-bund/it-rat/it-rat-node.html)).


### Nationaler Normenkontrollrat (NKR)
Der Normenkontrollrat ist unabhängig und hat eine beratende Funktion für Bundestag und Bundesrat inne. Seine zentrale Aufgabe ist es, Gesetzesentwürfe dahingehend zu prüfen, ob die dadurch entstehenden Kosten für Bürger:innen, Wirtschaft und Verwaltung nachvollziehbar sind. Zudem sollen diese auf mögliche Verwaltungsvereinfachung, sprich Bürokratieabbau, überprüft werden. Ab 2023 soll der NKR auch die digitale Ausführbarkeit eines geplanten Gesetzes zu prüfen. Der Normenkontrollrat wurde 2006 als unabhängiges Expert:innengremium eingesetzt und ist seit 2022 an das Bundesministerium für Justiz angegliedert. Er besteht aus 10 ehrenamtlichen Mitgliedern. Dieser veröffentlicht seit 2018 den Monitor für digitale Verwaltung, welcher die Umsetzung der Projekte kritisch beleuchtet. Zudem veröffentlicht er u.a. regelmäßig Gutachten zum Thema E-Government ([NKR](https://www.normenkontrollrat.bund.de/Webs/NKR/DE/der-nkr/aufgabe/aufgabe_node.html)).
Siehe dazu auch [BMJ.de](https://www.bmj.de/DE/Themen/RechtssetzungBuerokratieabbau/NKR/normenkontrollrat_node.html)


## Bereich Vollzug

### Länder und Kommunen
Wird Digitalisierung in Deutschland umgesetzt, müssen gezwungenermaßen auch föderale Strukturen thematisiert werden. Entsprechend können die Länder Politikgestaltung folgendermaßen aktiv beeinflussen:
* Einwirkung auf Bundesgesetzgebung über Bund-Länder-Arbeitsgruppen oder Bundesrat, Best-Practice Beispiele,
* Handlungsspielraum in der praktischen Umsetzung der Gesetze
* Landesgesetzgebungskompetenz für eigene oder ergänzende Bestimmungen bei nicht vorhandener Regelung durch den Bund (Art. 70 Abs. 2 GG)
* Einsatz eigener Ressourcen für weiterführende Projekte
* Landesspezifische Umstrukturierung von Diensten, Arbeitsprozessen und der internen Organisation [@Berzel_Länder]

Die Tatsache, dass Länder das Gestaltungsmandat zur Digitalisierung haben, wird kontrovers diskutiert. Während einerseits mit Innovation, Wettbewerb und Vielfalt und damit verbunden die „Generierung von Best Practices“ für eine föderale Struktur argumentiert wird, befürchten andere sogenannte „Lock-In-Effekte“, basierend auf der Pfadabhängigkeit von Länder- oder gar kommunenspezifischen Einzel- und Sonderlösungen [@Berzel_Länder] (Kursiv im Original). Neben der reinen Betrachtung von einzelnen Lösungen variieren die Länder zusätzlich in deren spezifischen Strategien, welche aufgrund des Wissens über die „politikfeldübergreifende“ Wirkung von Digitalisierung (Wirtschaft, Gesellschaft, Politik) aktiv auf Landesebene gestaltet werden muss [@Berzel_Länder]. Die logische Konsequenz sind dann Unterschiede in der ressortübergreifenden Realisierung von „Steuerungs- und Organisationsfragen“ [@Berzel_Länder]. Dies birgt zwar das Potenzial für die bereits genannten Musterbeispiele, in seiner Schlussbetrachtung bemerkt Alexander Berzel jedoch, dass „die Innovationsbilanz des deutschen Föderalismus [noch nicht] überzeugt“ [@Berzel_Länder]. Um also die Digitalisierungsbestrebungen von Bund und Ländern zu harmonisieren, zumindest in Form „niedrigschwelliger Standardisierung“ (Stichwort Interoperabilität), könnte eine rechtlich geregelte Kooperation, wie beispielsweise Art. 91 c GG (2009) und seinen späteren Ergänzungen helfen [@Berzel_Länder]. Die kurz skizzierten „dezentralisierte[r] Verwaltungsstrukturen sind verfassungsrechtlich gewollt“, und führten dieser Logik folgend zu einer Vielzahl digitaler Portale einzelner Kommunen und Länder, welche im Zuge des OZG in einem bundesweiten Portalverbund vernetzt werden sollen [@Martini_Bürgerkto]. Dies wiederum stellt Bund, Länder und Kommunen entsprechend vor die Herausforderungen, technische Lösungen für eine ursprünglich, jedoch nicht in Sachen Digitalisierung, gewollte Heterogenität.

## Bereich Koordination, Steuerung und Umsetzung (Zugangsgestaltung)

### IT-Planungsrat
„Der IT-Planungsrat ist das politische Steuerungsgremium von Bund, Ländern und Kommunen für Informationstechnik und E-Government” [@BMI_ITPR]. Dieser unterstützt Bund und Länder bei der Formulierung gemeinsamer Ziele und Handlungsfelder, beim Erarbeiten gemeinsamer Standards sowie bei der konkreten Umsetzung von Digitalisierungsprojekten. Ob diese Standards für das Angebot elektronischer Dienstleistungen umgesetzt wird, liegt weiterhin “allein in der Entscheidungskompetenz des Bundes und der jeweiligen Länder. Die Funktion des IT-Planungsrats ist daher allein die verwaltungsebenenübergreifende Planung und Koordinierung” [@Lühr_IT-PR].
„Gemäß § 1 IT-Staatsvertrag übernimmt der IT-Planungsrat folgende Aufgaben:
* IT-Koordination
Koordinierung der föderalen Zusammenarbeit in Fragen der Informationstechnik
* IT-Standards
Festlegung übergreifender IT-Interoperabilitäts-und Sicherheitsstandards
* Digitalisierung der Verwaltung
Koordinierung und Unterstützung von Bund und Ländern in Fragen der Digitalisierung von Verwaltungsleistungen
* E-Government-Projekte
Steuerung von zugewiesenen Projekten und Produkten des digital unterstützten Regierens und Verwaltens
* Verbindungsnetz
Koordinierungsgremium für das Verbindungsnetz zwischen den IT-Netzenvon Bund und Ländern“ [@IT-PR_Aufgaben]

Entsprechend ist dieses Gremium aktiv in die Umsetzung des OZG eingebunden [@Lühr_IT-PR].
Der IT-Planungsrat besteht im Kern aus insgesamt 17 Vertreter:innen der Bundesregierung und Länderregierungen. Zusätzlich dazu können weitere Personen, unter anderem aus den kommunalen Verwaltungen, der IT-Sicherheitsbeauftragte oder anderweitige Ansprechpartner:innen beratend teilnehmen. Darüber hinaus wird er von den folgenden Gremien in seiner Arbeit unterstützt: Abteilungsleiterrunde, Föderales Architekturboard, Arbeitsgruppe Cloud Computing und Digitale Souveränität, Arbeitsgruppe Informationssicherheit, Arbeitsgremium Verbindungsnetz, Kommunalgremium, Lenkungsgremium GDI-DE und dem Rechtsgremium (für weitere Informationen zu den einzelnen Gremien siehe [IT-planungsrat](https://www.it-planungsrat.de/)).

Entstanden ist der IT-Planungsrat im Rahmen der Föderalismusreform II (2009), wodurch eine intensivere Zusammenarbeit zwischen Bund und Ländern im Bereich IT beschlossen wurde. Seither arbeitet er als Nachfolger der Gremien Arbeitskreis der Staatssekretäre für E-Government in Bund und Ländern (Sts-Runde Deutschland-Online) und Kooperationsausschuss von Bund und Ländern für automatisierte Datenverarbeitung (KoopA ADV) [@Lühr_IT-PR].
Lühr zufolge hat der IT-Planungsrat seit seinem Bestehen eine Vielzahl an Projekten und Beschlüssen in die Wege geleitet, darunter die Nationale E-Government-Strategie (2010), zur Verdeutlichung der notwendigen Zusammenarbeit von Bund, Länder und Kommunen in Sachen Digitalisierung. Diese wurden in das 2013 in Kraft getretene E-Government-Gesetz integriert. Und zuletzt spielt der IT-Planungsrat eine wichtige Rolle in der Umsetzung des OZG, obgleich die Zielerreichung dessen in weiter Ferne liegt [@Lühr_IT-PR]. Entsprechend fällt das Fazit des ehemaligen Bremer Staatsrats Hans-Henning Lühr aus:
„Der IT-Planungsrat ist ein notwendiges Instrument zur Koordinierung der Digitalisierung der Verwaltungen von Bund, Ländern und Gemeinden, aber er kann nur dann zu einem entscheidenden Instrument für ein erfolgreiches E-Government in Deutschland werden, wenn er von einer Fehleranalyse ausgeht: Warum sind viele gute Ansätze immer wieder versandet, warum ist die erhoffte Resonanz bei den Bürger*innen nicht gefunden worden?“ [@Lühr_IT-PR].

### Fachministerkonferenz
Die Fachministerkonferenzen sind regelmäßige Treffen der Fachminister der Länder zum Austausch und der gemeinsamen Entscheidung zum Thema Verwaltungsdigitalisierung. Gemäß des Staatsvertrags stehen diese in engem Austausch mit dem IT-Planungsrat ([IT-Planungsrat: Fachministerkonferenz](https://www.it-planungsrat.de/foederale-zusammenarbeit/fachministerkonferenzen)).


### FITKO
Die Föderale IT-Kooperation (FITKO) wurde im Rahmen des ersten IT-Änderungsstaatsvertrags (2019) vom IT-Planungsrat ins Leben gerufen. Als agile Organisation soll sie die Digitalisierung der bundesdeutschen Verwaltung koordinieren und beschleunigen. Konkret unterstützt die FITKO die Zusammenarbeit aller föderalen Ebenen beim Erarbeiten einer gemeinsamen Umsetzungsstrategie und dem Aufbau effizienterer und simplifizierter Entscheidungsstrukturen. Die Konkreten Aufgaben der FITKO sind:
* Koordination: organisatorische und strategische Unterstützung des IT-Planungsrates, Programmmanagement der föderalen Umsetzung des OZG, Verwaltung des Digitalisierungsbudgets von Bund und Ländern
* Erarbeitung einer IT-Architektur mit Bund und Ländern mit standardisierten Schnittstellen und Datenformaten (siehe Föderales IT-Architekturboard)
* Unterstützung bei der Entwicklung und Umsetzung von Produkten und Projekten des IT-Planungsrats (z.B. FIT-Connect) 
* Bewirtschaftung des von Bund und Ländern gestelltem Digitalisierungsbudgets ([FITKO](https://www.fitko.de/ueber-uns/was-wir-tun))


### KosIT (Koordinierungsstelle für IT-Standards)
Die KosIT ist eine Konsequenz der Föderalismusreform II (2009) und ist 2011 aus der OSCI-Leitstelle hervorgegangen. Ihr Auftraggeber ist der IT-Planungsrat. Sie ist vorrangig für die Entwicklung und den Betrieb unterschiedlicher Standards für einen sicheren und ungehinderten Datentransfer in der öffentlichen Verwaltung zuständig. Zentrale Produkte sind das OSCI (Online Services Computer Interface) sowie die [XÖV-Standards](https://www.xoev.de/) (vgl. [egovernment Computing](https://www.egovernment-computing.de/was-ist-bzw-was-tut-die-kosit-a-984665/)).


### Föderales IT-Architekturboard
Das Föderale IT-Architekturboard ist ein Gremium bestehend aus den Vertreter:innen der FITKO, des BMI, der Länder sowie jeweils ein:e Vertreter:in und eine Stellvertretung für den kommunalen Bereich (ernannt von den kommunalen Spitzenverbänden). Es berät den IT-Planungsrat in allen Fragen rund um eine bundesweite IT-Architektur. Konkret entwickeln die Mitglieder des Architekturboards föderale IT-Architekturrichtlinien für laufende wie zukünftige IT-Architekturprojekte. Die Richtlinien richten sich dabei sowohl an Vertreter: innen der öffentlichen Verwaltung als auch an öffentliche wie private IT-Dienstleister. Ein zweiter Arbeitsbereich ist die Dokumentation der föderalen IT-Landschaft ([föderales IT-Architekturboard](https://www.fitko.de/foederale-koordination/gremienarbeit/foederales-it-architekturboard)).


## Bereich Sicherheit/Compliance

### Der Bundesbeauftragte für den Datenschutz und die Informationsfreiheit (BfDI)
Der BfDI ist eine eigenständige Bundesbehörde und bildet „die datenschutzrechtliche Aufsicht[ ] über alle öffentlichen Stellen des Bundes wie auch für bestimmte Träger der sozialen Sicherung“ ([bfdi.bund.de](https://www.bfdi.bund.de/DE/DerBfDI/Inhalte/DerBfDI/AufgabenBFDI.html)). Im Kontext der Verwaltungsdigitalisierung ist er besonders als Berater des Bundestages in datenschutztechnischen Fragen tätig.



### Bundesamt für Sicherheit und Informationstechnik
„Das Bundesamt für Sicherheit in der Informationstechnik (BSI) gestaltet Informationssicherheit in der Digitalisierung durch Prävention, Detektion und Reaktion für Staat, Wirtschaft und Gesellschaft. Das BSI ist die Cyber-Sicherheitsbehörde des Bundes und Gestalter einer sicheren Digitalisierung in Deutschland. Seit seiner Gründung 1991 hat sich das BSI zu einem ressortübergreifenden Kompetenzzentrum für Fragen der Informationssicherheit entwickelt, dessen fachliche Expertise national und international anerkannt ist.“ ([BSI](https://www.bsi.bund.de/DE/Das-BSI/Auftrag/auftrag_node.html))


### Datenschutzbeauftragte der Kommunen und sonstigen Vollzugsbehörden
Bei der konkreten Umsetzung von Digitalisierungsvorhaben in Form der Integration von IT-Anwendungen in einzelnen Behörden prüfen und begleiten in der Regel auch die internen Datenschutzbeauftragten diesen Prozess.

## Bereich Innovation

### Dit.bund (digital innovations team)
Das DIT ist eine agil arbeitende Arbeitsgruppe in Form eines Think-and-Do-Tanks. Ziel ist es diese Arbeitsweise einen Platz in Bundesbehörden zu bieten, um dort einen Raum für digitale Innovationen zu schaffen. Dabei wird konkret auf die Kooperation mit Partnerbehörden und die Befähigung von Verwaltungsmitarbeitenden zu agiler Arbeit abgezielt. Im Zentrum steht also die Vermittlung neuer und digitalisierter Arbeitsweisen ([öffentliche-it.de](https://www.oeffentliche-it.de/-/digital-innovation-team-dit-lehren-aus-der-praxis)).



### Digitalservice4germany
Die Digitalservice4Germany wurde 2020 als Bundes-GmbH gegründet und ist für die Entwicklung digitaler Anwendungen zuständig. Im Fokus stehen dabei Nutzer:innenbedürfnisse und agile Arbeitsmethoden ([digitalservice.bund.de](https://digitalservice.bund.de/)).


### Digitalakademie Bund
Die Digitalakademie ist seit 2021 Teil der Bundesakademie der öffentlichen Verwaltung des BMI. Diese ist für die Qualifizierung von Verwaltungsmitarbeitenden für alle Themen Rund um digitale Transformation zuständig ([digitalakademie.bund.de](https://www.digitalakademie.bund.de/DE/Home/home_node.html)).


### NEXT
Das Netzwerk: Experten für die digitale Transformation der Verwaltung (NEXT) beschreibt sich als „eine gemeinnützige Plattform aus der Verwaltung für die Verwaltung“ ([digitaler-staat.org](https://www.digitaler-staat.org/wp-content/uploads/2020/03/NExT_SE_2020.pdf)). Diese Vernetzung verschiedener Behörden, darunter die Bundeswehr, die Anstalt für Kommunale Datenverarbeitung in Bayern und weitere, dient dem gemeinsamen Erfahrungs- und Wissensaustausch zum Thema digitale Transformation ([vgl. ebd.](https://www.digitaler-staat.org/wp-content/uploads/2020/03/NExT_SE_2020.pdf)).


### ÖFIT
Das Kompetenzzentrum Öffentliche IT ist an das Fraunhofer-institut für offene Kommunikationssysteme angegliedert und setzt sich mit Fragestellungen hinsichtlich einer sich weiterentwickelten Digitalisierung und damit einhergehenden Wechselwirkungen mit Gesellschaft auseinander. Mit Ihren Erkenntnissen dient sie der Politik und Verwaltung als beratende Institution. Zentral in den Untersuchungen der ÖFIT sind demnach ableitbare „Gestaltungs- und Regulierungsanforderungen“ ([oeffentliche-it.de](https://www.oeffentliche-it.de/)). 

Neben den Verwaltungen sind als weitere einflussnehmende Player in den letzten Jahren unabhängige Organisationen, Forschungs- und Innovationseinrichtungen hinzugekommen. Diese arbeiten an neuen technischen Möglichkeiten, teilweise gezielt für die Verwaltungsmodernisierung. Obwohl diese eine wichtige Rolle in der Entwicklung technischer Neuheiten einnehmen, schreibt der Normenkontrollrat diesen Institutionen nur ein mäßiges treibendes Potenzial zu, zumal diese die organisatorische Einbettung häufig dabei vernachlässigen. Damit kommt es zu Skalierungsproblemen, da diese nicht flächendeckend (nach-)genutzt werden (können). Zudem fehlt es dabei ausreichenden Ressourcen.

## Privatwirtschaftliche Dienstleistungsunternehmen
Für private IT-Dienstleister und Beratungsunternehmen, bildet die Verwaltungsmodernisierung eine lukrative Einnahmequelle. Sei dies auf Basis neuer Produkte und aus der Wirtschaft entlehnte Beratungs- oder Organisationsansätze, oder in Form von so genannten Rahmenverträgen, über die Verwaltungen ohne vergaberechtliche Hemmnisse Ressourcen abrufen können. Inwieweit diese privaten Akteure tatsächlich die Verwaltungsdigitalisierung richtig auf den Weg bringen, soll an dieser Stelle nicht bewertet werden. Feststeht jedoch, dass in vielen Verwaltungen aufgrund des Fachkräftemangels ohne diese Akteure keine nennenswerte Digitalisierung erreicht werden kann.

## Verwaltungsmitarbeitende
Zuletzt spielen auch die Menschen in den Behörden der verschiedenen Ebenen und Sachbereichen mehrfach eine wichtige Rolle. Zum einen verfügen nicht alle (kommunalen) Behörden über die gleichen finanziellen oder personellen Ressourcen, wodurch die OZG-Umsetzung, inkl. der EfA-Projekte, für viele von vornherein nicht leistbar war [@NKR_Bericht2021]. Zum anderen beeinflussen auch Faktoren, wie Akzeptanz und Bereitschaft zur Veränderung, Verantwortungsabgabe o.ä. ein solche Modernisierungsvorhaben. Ein Aspekt der nicht erst seit der OZG-Umsetzung bekannt ist, aber zu wenig berücksichtigt wurde. Dem Monitorbericht (2021) des Normenkontrollrats zufolge lässt sich erst zum Zeitpunkt der Veröffentlichung eine Annäherung in Form einer positiven Basis der Zusammenarbeit feststellen. Eine langsam wachsende Bereitschaft zur Kooperation erfordert jedoch in einem weiteren Schritt die Einigung und das Festhalten einer gemeinsamen Vision, damit u.a. groß angelegte Projekte, wie die OZG-Umsetzung, erfolgreich werden [@NKR_Bericht2021].


## Steuerungsproblematik

Die Akteure, die z.B. das OZG umsetzen müssen (siehe dazu Wimmelbild), verfolgen als öffentliche Leistungsnetzwerke das gemeinsame Ziel des kooperativen, vernetzten staatlichen Handelns, um u.a. einen vereinheitlichten und vereinfachten Zugang zu Verwaltungsleistungen zu gewährleisten. Das Management von Netzwerken bringt einen hohen organisatorischen Aufwand mit sich, welcher die Frage nach passenden Steuerungsmechanismen in einem föderalen Mehrebenensystem aufwirft und birgt in der Praxis verschiedene Herausforderungen. Konkret sind folgende Aspekte zu betrachten:  
Zum einen organisieren sich Netzwerke in flachen Hierarchien, so dass mikropolitische Ziele und Interessen einzelner Akteure keiner strikten Kontrolle unterliegen. Stattdessen müssen diese immer wieder neu in verschiedenen Arenen verhandelt werden (siehe dazu auch „Mikropolitisches Arenenmodell“ nach Brüggemeier [@bruggemeier]). Zum anderen gibt es in Netzwerken eine weniger stark (oder keine) ausgeprägte Weisungsbefugnis. Entsprechend werden Verträge zwischen den Akteuren auf Augenhöhe ausgehandelt und basieren nicht wie bisher auf einer klassischen Legitimationskette, wodurch auch die Verantwortungsträger neu bestimmt werden müssen. Aus diesem Grund muss erstens eine Vertrauensbasis zwischen den beteiligten Akteuren hergestellt werden, um eine Verhandlung der relationalen Verträge auf Augenhöhe überhaupt zu gewährleisten. Zweitens müssen diese auch koordiniert und den Verantwortungsbereiche zugeordnet werden. Grundlegende Koordinationsmechanismen aus der Organisationsforschung sind u.a. die direkte Weisung, Programme, Pläne, Märkte, die Professionalisierung und die Selbstabstimmung [@Prozessmanagement]. Netzwerke koordinieren sich meist in Selbstabstimmung, welche innerhalb institutionalisierter Arenen koordiniert werden. Beispiele für solche Arenen im OZG-Kontext sind u.a. Ausschüsse, Abteilungsleiterkonferenzen, Koordinatoren, oder Integrationsstellen. Zusammengefasst: Die Umsetzung einer stark vernetzten öffentlichen Verwaltung und eines föderalen One-Stop-Zugangs verspricht einen hohen Nutzen für Bürger:innen und Unternehmen. Gleichzeitig steigt mit zunehmender Vernetzung auch der Koordinations- und Steuerungsaufwand für den Austausch und Verhandlungen zwischen den Akteuren sowie für veränderte Machtstrukturen und Standardisierungsabsprachen. Man kann daher von einem Vernetzungsdilemma sprechen, dass es bei der weiteren Digitalisierungsbestrebungen zu berücksichtigen gilt.

## Quellen
