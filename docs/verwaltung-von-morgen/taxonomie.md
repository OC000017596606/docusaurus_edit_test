---
sidebar_position: 2
sidebar_label: Spürbarkeit der Verwaltungsinteraktion
description: Ansatz zur Bestimmung der Nutzerfreundlichkeit des Verwaltungszugangs 
---

# Spürbarkeit der Verwaltungsinteraktion - Leitbilder des Verwaltungszugangs (Heuristische Taxonomie der Spürbarkeit der Verwaltung)

Spürbarkeit von Verwaltung gilt als zentrales Prinzip bei der Neugestaltung des Verwaltungszugangs und beschreibt, wie Nutzer:innen die Interaktion mit der Verwaltung wahrnehmen („Nutzer:innenperspektive“) [@2011:brggemeier:brokrati]. Integrierte Verwaltungszugänge, z.B. über Verwaltungsportale, sind eine Möglichkeit, Einfluss darauf zu nehmen, ob und in welchem Maße Bürger:innen und Unternehmen den Kontakt mit Verwaltung spüren oder nicht spüren.

### Hintergrund

Häufig erfolgt der Kontakt mit Verwaltung aus einer Pflicht- oder Zwangssituation heraus (z.B. Verlängerung des Personalausweises). Aus diesem Grund spielt die Spürbarkeit im Sinne des wahrgenommenen Interaktionsaufwands eine wichtige Rolle für die Zugangsgestaltung.

Ziel öffentlicher Verwaltungen ist es, die administrativen Lasten so weit wie möglich bzw. nötig zu reduzieren (d.h. Bürokratieabbau zur Sicherung staatlicher Handlungsfähigkeit und Förderung demokratiefördernder Aspekte). Dabei steht die Frage im Raum, ob der beste Zugang keine Verwaltungsinteraktion ist. Demnach wäre die noch so nutzerorientierte Zugangsgestaltung womöglich nur die zweitbeste Lösung. Da dies nicht immer gelingt bzw. nicht sofort, sind Abstufungen der Spürbarkeit erforderlich (siehe dazu [eGovCampus: Bürokratiearme Verwaltungsinteraktion](https://egov-campus.org/courses/verwaltungsportale_up_2021-1)).

Voraussetzung für die möglichst nicht-spürbare Verwaltung ist eine weitgehende Integration von Daten von Bürger:innen und Unternehmen, die bereits an unterschiedlichen Stellen innerhalb der öffentlichen Verwaltung vorliegen und für die Bearbeitung erforderlich sind (sog. Once-Only-Prinzip) [@2011:brggemeier:brokrati].

### Konzept: Heuristische Taxonomie

Ein Analysekonzept der Gestaltungsrahmen für die Abstufung bei der Zugangsgestaltung stellt die „Heuristische Taxonomie der Spürbarkeit der Verwaltung“ von Brüggemeier dar (2011). In der Typologie sind Ausprägungen von sehr hohem Interaktionsaufwand im „Go-Government“ (Traditionelles Leitbild – Adressat initiiert für jedes Verwaltungsanliegen den Kontakt) bis zum Verschwinden des Interaktionsaufwands im „No-Government“ („Neu“ – nicht-spürbares Verwaltungshandeln und Abschaffung von Verwaltungsportalen für Adressaten) abgebildet. Um die verschiedenen Leitbilder zuzuordnen, steht dem Interaktionsaufwand parallel verlaufend der Grad der Bündelung (i.S.v. Grad der Zugangsintegration) und der Grad der Proaktivität von Verwaltungsleistungen gegenüber. 

![Taxononomie der Spürbarkeit](/assets/Rahmen/Heuristische_Taxonomie__Bgm._.png)

Das Leitbild der gebündelten Zugangsintegration wird auch als One-Stop-Verwaltung mit unterschiedlichen Entwicklungsstufen beschrieben. Der [Portalverbund](docs/it-landschaft/portale/pvog) im [OZG-Kontext](docs/grundlagen-und-rahmen/ozg) ist als „Föderale One-Stop-Verwaltung“ einzuordnen.

Eine steigende Datenintegration ermöglicht es der Verwaltung, Leistungen immer mehr auch proaktiv („Aufsuchende Verwaltung“ und „Zuvorkommende Verwaltung“) und sogar antizipativ anzubieten („No-Stop-Verwaltung“). Unter dem Motto: „Ihr Personalausweis läuft in sechs Wochen ab – hier können Sie Ihre Daten bestätigen/ändern und ein neues Foto übermitteln“. Fragen der Proaktivität von Verwaltung sind jedoch nicht nur rein technischer Natur, sondern müssen vor allem im Bewusstsein für die zukünftige Leistungserbringung politisch verhandelt und verankert werden.
Anzumerken ist, dass die beschriebenen Leitbilder stets hinsichtlich des Zielbilds eines vereinfachten, leicht durchführbaren und bürokratiearmen Verwaltungszugangs zu hinterfragen sind. Zudem müssen auch anliegensspezifische Zugänge bei der Bewertung der verschiedenen Leitbilder berücksichtigt werden, da sich diese in der Regel selten für alle Verwaltungsleistungen gleichermaßen eignen [@2011:brggemeier:brokrati].


## Quellen
