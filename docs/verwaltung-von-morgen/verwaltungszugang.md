---
sidebar_position: 3
descrition: Zusammenhang von Zugang und Leistungserstellung
---

# Verwaltungszugang

Veränderungsimpulse für Verwaltungshandeln orientieren sich meist an dem Ziel einer verbesserten Nutzer:innenfreundlichkeit. Die Gestaltung des Verwaltungszugangs hängt unmittelbar mit der Produktionsgestaltung zusammen. Aus diesem Grund ist eine technische und organisatorische Reflexion der Geschäftsprozesse zur Erstellung einer Leistung zwingend notwendig.

### Hintergrund

Die Frage, wie der Zugang zu Verwaltungsleistungen für Bürger:innen einfacher und besser gestaltet werden kann, wird bereits seit 1971 diskutiert. Auch das physische Bürgerbüro mit Leistungen verschiedener Verwaltungsbereiche wurde in dieser Zeit „erfunden“.
Bereits damals gab es auch Überlegungen, wie Informationstechnik Bürger:innen und Unternehmen den Zugang zu Verwaltung erleichtern kann. Was zu dieser Zeit mit „Die Daten müssen laufen, nicht die Bürger“ als Leitbild für Verwaltungsbestrebungen gefordert wurde, hat sich über verschiedene Stufen der Zugangsintegration und technischer Möglichkeiten bis heute stetig weiterentwickelt [@goller]. Dabei kann nach Lucke (2008) zwischen typisierten Ausbaustufen der Zugangsintegration zum Verwaltungszugang unterschieden werden [@lucke]. 

### Ausbaustufen

Das Spektrum reicht dabei von einfachen Einstiegspunkten und Informationssammelstellen für Bürger:innen, um eine erste Orientierung mit grundlegenden Informationen gebündelt zu erhalten (z.B. Informationsseiten von Kommunen – Zeit des „Virtuellen Rathauses“ bzw. „Grußwort des Bürgermeisters“) bis hin zu echten Service-Stellen, in denen fachlich zusammenhängende Verwaltungsangebote gebündelt sind (z.B. klassische Bürgerämter, Call-Center, Internetportale).

Seit Inkrafttreten des Onlinezugangsgesetzes (OZG) im Jahr 2017 wurde die Weiterentwicklung der Zugangsintegration auf echte Service-Cluster für Verwaltungszugänge politisch beschlossen und als „Portalverbund“ verfolgt. Ziel ist es, Service-Stellen miteinander zu vernetzen und einen einheitlichen Verwaltungszugang für Bürger:innen und mittlerweile auch Unternehmen zu ermöglichen (Single-Window-Prinzip). Im Vordergrund steht dabei stets die Verbesserung der Nutzer:innenfreundlichkeit im Sinne der Zielgruppenorientierung und die Akzeptanz der angebotenen Verwaltungszugänge. Dafür werden integrierte Zugangsoptionen (auch häufig Multi-Kanal-Zugang) zu Verwaltungsleistungen gefordert, so dass Bürger:innen und Unternehmen verschiedene Zugangsoptionen für Verwaltungsleistungen und Partizipationsmöglichkeiten haben (persönlich, postalisch, telefonisch, digital).

Um eine solche Vernetzung erst zu ermöglichen, muss neben der Frage der Zugangsgestaltung zusätzlich die Frage der Produktionsgestaltung (Wie produziere ich die Verwaltungsleistungen, die über den Verwaltungszugang angeboten werden?“) beantwortet werden. Beide Fragen sind eng mit technischen und organisatorischen Gestaltungsanforderungen verknüpft. Ein einheitlicher Verwaltungszugang im föderalen Mehrebenensystem benötigt daher den Blick auf eine gemeinsame digitale Infrastruktur, eine konsequente Geschäftsprozesssicht auf der Arbeitsebene, die Trennung von „Zugang“ und „Produktion“ im Leistungsprozess und vor allem eine intensive Kooperation zwischen den Akteuren (siehe dazu eGovCampus: Verwaltungsportale – Einführung in den Kurs). Pointiert zusammengefasst: Ein einheitlicher Verwaltungszugang benötigt eine vernetzte Leistungsproduktion. Verwaltungsportale sind damit vernetzte öffentliche Leistungsnetzwerke [@Leistungserstellung].

## Quellen
