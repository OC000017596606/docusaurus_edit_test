---
sidebar_position: 3
description: PVOG als Verbindene Komponente der Bundes- und Länderportale
---

# Portalverbund Online-Gateway 

## Allgemeines

Der Portalverbund Online-Gateway (PVOG) bildet die föderale Struktur Deutschlands ab und wird vom Portalverbund zur Verfügung gestellt. Mittels dieser Plattform können Bürger:innen und Unternehmen jede Verwaltungsleistung – unabhängig davon, auf welchem Verwaltungsportal in Deutschland sie die Suche nach der Verwaltungsleistung beginnen – einfach und schnell erreichen. [@BmihPvogArchitektur] Dafür müssen der Bund und jedes Land je ein Verwaltungsportal bereitstellen, das über eine Suchkomponente, eine Bezahlkomponente, ein Nutzerkonto und eine Postfachfunktion verfügt. [@BmihPvogArchitektur] Die Länder sind darüber hinaus für die Anbindung ihrer Kommunen und die Errichtung von Landesportalverbünden verantwortlich. Die Verknüpfung dieser Portale wird technisch mittels des Online-Gateway Portalverbunds realisiert, welcher durch die [FITKO](https://www.fitko.de/) betrieben wird. Im Mittelpunkt der Umsetzung steht die Nutzer:innenorientierung, sowohl hinsichtlich der antragstellenden Person oder Institution als auch mit Blick auf die ausführenden Behörden. Alle am Verbund teilnehmenden Portale sind gleichberechtigte Partner. Die Ausführung der Verwaltungsleistung erfolgt bei der zuständigen Behörde. Die Leistung kann auf einer eigenen Website angeboten werden, die über die Verwaltungsportale des Portalverbunds aufgerufen wird, oder direkt in einem der Verwaltungsportale des Portalverbunds.
Es sind bereits die Redaktionssysteme des Bundes und aller Länder mit Ausnahme des Saarlandes an den Sammlerdienst des PVOGs angeschlossen. Darüber hinaus findet eine EU-weite Anbindung des PVOGs an das europäische Portal [Your Europe](docs/it-landschaft/portale/bund-und-laender) statt. [@FitkoPvog]

![](/assets/IT-Landschaft/standards-schnittstellen-portalverbund.png)

Die Federführung für die Umsetzung des Portalverbunds liegt beim Bundesministerium des Innern und für Heimat ([BMI](https://www.bmi.bund.de/DE/startseite/startseite-node.html)). [@BmihPvogArchitektur] Die Festsetzung der Rahmenbedingungen und die Betreuung des Projekts erfolgt durch den IT-Planungsrat ([Entscheidung 2018/40](https://www.it-planungsrat.de/beschluss/beschluss-2018-40)). Dabei wird darauf geachtet, dass der Portalverbund den Vorgaben der [EU-Verordnung zum Single Digital Gateway (SGD)](https://eur-lex.europa.eu/legal-content/DE/TXT/PDF/?uri=CELEX:32018R1724&from=DE) entspricht. [@BmihPortalverbund]

![](/assets/IT-Landschaft/portalverbund-einleitung.png)

## Nutzung

Die durch das PVOG gesammelten Daten können auf drei verschiedenen Wegen abgerufen werden:
1. über die API des Suchdienstes [@FitkoPvog]
2. als Komplett-Export über den Bereitstelldienst [@FitkoPvog]
3. Verwendung des [Suchclients](https://servicesuche.bund.de/#/de/)

## Dienste und APIs

Der PVOG umfasst drei Dienste, welche in den folgenden Unterabschnitten näher vorgestellt werden. Zu jedem Dienst existiert eine REST-API Schnittstelle, die den Standard OpenAPI 3.0 erfüllt. Die Dienste kommunizieren zusätzlich mit einem Virenscanner, welcher neue XZuFi-Dateien auf Viren prüft, bevor sie in das System eingepflegt werden. [@DataportKonzeptSammlerdienst] 

### Sammlerdienst 

Der Sammlerdienst ist für das Aufnehmen sowie das Aufbereiten von Verwaltungsleistungen aus den Redaktionssystemen verantwortlich. [@ItPlanungsratProdukte] Die Komponenten des Dienstes sind in Java implementiert und laufen auf einem Apache Tomcat Application Server. Dieser wird wiederum in einer Java-Laufzeitumgebung (JVM) auf Ubuntu betrieben. Dies kann in Form eines Docker-Containers in einem Docker-Engine ausgeführt werden. Für die Speicherung der im Dienst verarbeiteten Daten wird das relationale Datenbankmanagement-System (RDBMS) PostgreSQL verwendet. [@DataportKonzeptSammlerdienst] 
Der Zugriff des Sammlerdienstes auf Redaktionssysteme kann anonym oder abgesichert über HTTP BasicAuthentication erfolgen. Dies liegt im Ermessen des Betreibers des Redaktionssystems. Die Übertragung erfolgt über eine verschlüsselte Verbindung, wobei die einzelnen XZuFi-Dateien nicht verschlüsselt oder signiert werden. [@DataportKonzeptSammlerdienst]
Weitere Informationen zu dem Dienst können in [@DataportKonzeptSammlerdienst] [@DataportSammlerdienst] gefunden werden. Die Schnittstellenbeschreibung ist [hier](https://anbindung.pvog.cloud-bdc.dataport.de/service-sad/rest.html) verfügbar.

### Bereitstelldienst 

Der Bereitstelldienst erhält die von unterschiedlichen Sammlerdiensten gesammelten Daten von der Ereignisverwaltung, führt diese zusammen, verwaltet sie und bringt Schnellkorrekturen an Onlinediensten in den Bestand ein. Er ist die zentrale Stelle für den Zugriff auf den aktualisierten Gesamtbestand. [@ItPlanungsratProdukte] Weitere Informationen zu dem Dienst können in [@DataportBereitstelldienst] gefunden werden. Die Schnittstellenbeschreibung ist [hier](https://anbindung.pvog.cloud-bdc.dataport.de/service-bed/rest.html) verfügbar.

### Suchdienst 

Der Suchdienst macht Verwaltungsleistungen für externe Konsument:innen anwendergerecht durchsuchbar und auffindbar. [@ItPlanungsratProdukte] Dafür kann die REST-API oder der Such-Client verwendet werden (siehe Kapitel Nutzung). [@FitkoKonzeptSuchdienst] Weitere Informationen zu dem Dienst können in [@FitkoKonzeptSuchdienst][@DataportSuchdienst] gefunden werden. Die Schnittstellenbeschreibung ist [hier](https://anbindung.pvog.cloud-bdc.dataport.de/service-sud/rest-22.html) verfügbar.

## Sicherheit

### Sicherheitspolicies

In regelmäßigen Zyklen werden technische Schulden analysiert und möglichst zeitnah abgebaut. Um Konsistenz zwischen den Komponenten zu gewährleisten, wird die Analyse gegen sprach-spezifische Quality-Gates bzw. Regeln durchgeführt. Dazu zählt unter anderem die Verwendung von statischen Analyse-Tools zur Sicherung der technischen Qualität als Teil der CI/CD Pipeline. [@DataportPortalverbund]

### Softwareentwicklung & Implementierung

Der komplette Quellcode aller Dienste von "Suchen & Finden" sowie aller weiterer unterstützenden Komponenten findet sich auf dem selbst-gehosteten nicht-öffentlichen GitLab-Server. Alle unmittelbar entwicklungsbezogenen Aktivitäten laufen über GitLab, wie z.B. Merge Requests und die Release-Verwaltung. Das umfangreiche System zur kontinuierlichen Integration bzw. automatischen Bereitstellung wird für die meisten Projekte komplett genutzt, um eine agile Vorgehensweise und fortlaufende Releases zu gewährleisten. [@DataportPortalverbund]

### Autorisierung und Authentifizierung

Die Kommunikation im und mit dem Portalverbund Online-Gateway ist über OAuth2 gesichert. Im Rahmen des Portalverbundes wird hierfür Keycloak als Authentifizierungsserver verwendet. Um die Schnittstellen des Portalverbundes abrufen zu können, muss eine Registrierung als Client beim Keycloak erfolgen. [@DataportKonzeptSammlerdienst] [@FitkoKonzeptSuchdienst]

### Updates
Es sind in den letzten Jahren mindestens einmal bis mehrmals jährlich Updates für die Dienste bereitgestellt worden. Im Changelog können die vorgenommenen Änderungen nachvollzogen werden. Durch die eingesetzte Master-Slave Architektur sind Rolling Updates beim Suchdienst möglich. [@DataportArchitektur]

### Kryptografie
Die Verschlüsselung der Übertragungswege erfolgt auf Basis von TLS (> Version 1.1). [@DataportKonzeptSammlerdienst] [@FitkoKonzeptSuchdienst]

### Datenhaltung 
Die Dienste nutzen den Standard XZuFi 2.1 & 2.2. Dieser standardisiert den von Produkt und Hersteller unabhängigen Austausch von Informationen zu Verwaltungsdienstleistungen, Gebieten, Formularen und den hierfür zuständigen Organisationseinheiten im Kontext von Zuständigkeitsfindern, Bürger- und Unternehmensinformationssystemen und Leistungskatalogen. [@DataportPortalverbund]
Alle 24 Stunden werden die kompletten Daten aller angeschlossenen Redaktionssysteme abgerufen, validiert und in das PVOG eingespielt [@FitkoSammelnSuchenFinden]. Für die Speicherung der im Dienst verarbeiteten Daten sowie des zentralen Loggings wird Elastic Stack und PostgreSQL verwendet. [@DataportKonzeptSammlerdienst] [@DataportPortalverbund]

### Datenschutz
Die Datenschutzerklärung kann [hier](https://public.demo.pvog.cloud-bdc.dataport.de/#/de/privacy) abgerufen werden. Der behördliche Datenschutzbeauftragte für das Projekt ist Björn Canders.

### Responsible Disclosure
Es sind keine Informationen zu Responsible Disclosure oder dem IT-Sicherheitsbeauftragten vorhanden.

## Schnittstellen zu anderen Themen
Das PVOG ist in den Expert:innengruppen von FIM vertreten und definiert zentrale Anforderungen für die Weiterentwicklung des Standards XZuFi.
Weiterhin wird das PVOG um Zuständigkeitsinformationen für das Antragsrouting via FIT-Connect erweitert. Dies findet im Rahmen eines vom föderalen IT-Architekturboard angestoßenen Pilotprojekts für die Parametrisierung der "Einer für Alle"-Onlinedienste statt.
Perspektivisch kann das PVOG die separate Datenhaltung in der Wissensmanagement-Plattform des 115-Verbundes ablösen. Dadurch würden auch die Redaktionssysteme entlastet, die momentan separate Exporte für PVOG und 115 zur Verfügung stellen. Aktuell wird im Rahmen der Weiterentwicklung der 115 die Eignung des PVOG als Datenquelle für Chatbots evaluiert.
Die im PVOG nachgewiesenen Daten bilden die Grundlage für die Visualisierung des Fortschritts bei der Umsetzung des Onlinezugangsgesetzes ([OZG](docs/grundlagen-und-rahmen/ozg)) im OZG-Dashboard. [@FitkoPvog]

## Quellen
