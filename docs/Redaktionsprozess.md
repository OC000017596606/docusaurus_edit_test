---
sidebar_position: 7
---

# Guideline für kollaboratives Arbeiten

Verwaltungsdigitalisierung befindet sich in fortdauernder Entwicklung. Was heute gilt, ist morgen vermutlich schon veraltet. Damit dies nicht mit dem gesammelten Wissen in unserem Kompass der föderalen IT-Architektur geschieht, möchten wir es gemeinsam mit Ihnen am Leben halten.

**Wir laden Sie dazu ein aktiv mitzuschreiben!** 

Doch auch diese Form der freien Zusammenarbeit benötigt ein paar Grundregeln sowie einen geordneten Redaktionsprozess. Wir haben versucht, die verschiedenen Formen der Mitarbeit möglichst leicht zugänglich zu gestalten.
Grundsätzlich können Sie
1. über eine Kommentarfunktion die vorhandenen Inhalte diskutieren und somit eine Überarbeitung dieser anstoßen.
2. die einzelnen Kapitel selbst überarbeiten und über einen Klick direkt in den Redaktionsprozess mit einfließen lassen.
3. einen Vorschlag für einen weiteren Artikel bei uns einreichen.
Wie dies genau funktioniert, erfahren sie im weiteren Verlauf.

## Grundregeln
Da es sich bei dem Kompass der föderalen IT-Architektur auch weiterhin um Ergebnisse aus einem wissenschaftlichen Projekt handelt, soll dieser Standard auch im kollaborativen Arbeiten gewährleistet sein. Konkret bedeutet dies:
* Grundsätzlich müssen Änderungen, Anmerkungen und Diskussionsbeiträge mit dem Namen des:der Autor:in gekennzeichnet sein
* Die Inhalte Ihrer Beiträge müssen deutlich als Meinung oder nachweisbares Wissen erkennbar sein. Daher sind Quellenangaben nach APA verpflichtend.
* Jegliche Beiträge, die dies nicht erfüllen, können in unserem Redaktionsprozess nicht berücksichtigt werden.
* Beiträge, die sich jeglicher konstruktiven Kritik verweigern oder in irgendeiner Form menschen- bzw. demokratiefeindlich sind, werden ohne Verwarnung gelöscht. Der Kompass der föderalen IT-Architektur soll Hass und Hetze keine Plattform bieten.

#### Kommentieren
* Wenn Sie derzeit die Artikel im GitLab ansehen, können sie darunter ein sog. Issue erstellen. Dies ermöglicht es Ihnen, den Artikel zu kommentieren.
* Für die Zukunft ist Folgendes geplant:
    * Unter jedem Artikel im Kompass der föderalen IT-Architektur befindet sich ein Button [Bezeichnung], der Sie auf eine Kommentarseite führt. Dort befindet sich ein Feld für Ihren Beitrag (Freitext) sowie Pflichtfelder für Ihren Namen und Ihre E-Mail-Adresse (beide Informationen dienen lediglich dem Redaktionsteam und werden nicht öffentlich gezeigt).
    * Nach Absenden Ihres Kommentars erscheint dieser als Beitrag in dem jeweiligen Artikelbezogenen Forum.

#### Artikel bearbeiten
* Unter jedem Artikel im Kompass der föderalen IT-Architektur befindet sich ein Button [Bezeichnung], der Sie auf eine Seite führt, auf der Sie die jeweiligen Texte bearbeiten können.
* Dort müssen Sie zudem Ihre Autor:innenschaft bestätigen, indem Sie Vor- und Zuname in die dafür angelegten Felder eintragen. Ihre Daten werden lediglich abgekürzt unter ihrem veröffentlichten Beitrag stehen.
* Außerdem bitten wir Sie, Ihre E-Mail-Adresse anzugeben, sodass wir sie bei Veröffentlichung/Ablehnung benachrichtigen können.
* Der Redaktionsprozess erfolgt manuell, wodurch jede Veröffentlichung sowohl inhaltlich als auch bezüglich des Schutzes Ihrer Daten durch unser Team geprüft wird.

#### Artikel hinzufügen (in Planung)
* Möchten Sie einen weiteren Artikel hinzufügen, können Sie uns diesen gerne über den folgenden Link einreichen
* Dazu bitten wie Sie, auch hier Name, Vorname, E-Mail-Adresse sowie die gewünschte Position des Artikels in der Gliederung in die dafür vorgegebenen Textfelder einzutragen.

#### Quellenangaben
* Zur Sicherung der Wissenschaftlichkeit im Sinne nachweisbarer und nachvollziehbarer inhaltlicher Angaben/Statements, möchten wie Sie bitten, die folgenden Regeln zu beachten:
    * Direkte Zitate sind in Anführungszeichen zu setzen.
    * Bei übernommenen Informationen bitte im Fließtext auf die Quelle verweisen.
        * Dafür setzen Sie am Ende des Satzes/Abschnitts eine [ ] und fügen dort eine Ziffer nach der jeweiligen Nummerierung in Ihrem Quellenverzeichnis ein, beginnend bei eins.
        * Ihr erster Quellenverweis lautet also „[1]“
* Am Ende Ihres Beitrags bitten wir Sie, die genutzten Quellen aufzulisten.
    * Nummerieren Sie diese bitte gemäß den Quellenangaben im Fließtext durch.
    * Handelt es sich um Onlinequellen, fügen sie dahinter in ( ) das Datum, wann Sie diese zuletzt aufgerufen haben: (zuletzt aufgerufen am tt.mm.jjjj)
* Für Hinweise zur Auswahl des richtigen Quellentyps und der entsprechenden korrekten Quellenangabe siehe u.a. Bachmann und Theel (Hrsg.), 2021, S. 38-92.
* Für weitere Informationen verweisen wir auf Bachmann und Theel (Hrsg.), 2021.

Quelle: Bachmann, H. & Theel, M. (Hrsg.) (2021). Die deutschen APA-Richtlinien: Basierend auf der 7. Auflage (2019) des offiziellen APA-Publication-Manuals. Scribbr. https://www.scribbr.de/zitieren/handbuch-apa-richtlinien/ (Aufgerufen am 23.01.2024)
