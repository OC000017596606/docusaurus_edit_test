---
sidebar_position: 4
description: Einführung und Kontextualisierung OZG
---

# Onlinezugangsgesetz (OZG)

## Genese, verfassungsrechtliche/europarechtliche Einordnung und Intention
Als das Grundgesetz (GG) 1949 in Kraft trat, waren Fragen der informationstechnischen Vernetzung von Verwaltungen noch kein relevanter Regelungstatbestand. In den folgenden Jahrzehnten entwickelten sich lose Zusammenarbeitsstrukturen zwischen Bund und Ländern, die jedoch erst 2009 Eingang in die Verfassung fanden (vgl. Art. 91 c GG / Gemeinschaftsaufgabe Informationstechnik). Verschiedene Bausteine einer föderalen IT-Architektur sind bereits vorgedacht bzw. existent (z.B. Zuständigkeitsfinder, Deutsches Verwaltungsdiensteverzeichnis (DVDV), Fachverfahren, e-Akten etc.), allerdings noch nicht in der Vernetzungsdimension, die das Onlinezugangsgesetz (OZG) verfolgt. Im Rahmen der Neuregelung der Bund-Länder-Finanzbeziehungen hat man sich 2017 auf eine Ergänzung des Art. 91 c GG verständigt [@Martini_91c]. Der neu eingefügte Absatz 5 regelt den übergreifenden informationstechnischen Zugang zu Verwaltungsleistungen von Bund und Ländern zugunsten des Bundes. Der Bund hat von seiner ausschließlichen Gesetzgebungskompetenz Gebrauch gemacht und diesen Regelungsauftrag mit dem OZG ausgefüllt. Einige der zu digitalisierenden OZG-Leistungen fallen schließlich auch unter die Single-Digital-Gateway-Verordnung (SDG-VO) der Europäischen Union, die bis Ende 2023 umgesetzt sein muss. Die SDG-VO und das OZG verfolgen dasselbe Ziel: ein einheitliches digitales Zugangstor zu Verwaltungsleistungen.

## Regelungsinhalt und komplementäre Vorhaben
Bund und Länder werden durch das OZG verpflichtet, ihre Verwaltungsleistungen bis 31.12.2022 elektronisch über Verwaltungsportale anzubieten (vgl. § 1 Abs. 1 OZG) und diese Portale zu einem Portalverbund zu verknüpfen (vgl. § 1 Abs. 2 OZG). Das OZG ist als Motornorm für einen längerfristig angelegten Transformationsprozess hin zu einer digital vernetzten Verwaltung zu verstehen. Komplementäre Vorhaben – wie etwa die Registermodernisierung – sind in diesem Kontext zu betrachten: Eine der Grundvoraussetzungen für vernetzte Verwaltungsprozesse ist die digitale und interoperable Datenhaltung, die Informationsaustausch in Echtzeit ermöglicht und redundante Datenerfassung vermeidet (Once-Only-Prinzip) [@BWLöffentlicheVerwaltung].

## Portalverbund, Reifegradmodell und Servicestandard
Verpflichtende Bestandteile der Portale sind ein interoperables Nutzerkonto, eine Such- und Bezahlkomponente und ein Postfach [@IT-PR_Dig]. Beim Portalverbund handelt es sich lediglich um eine technische Verknüpfung der heterogenen und dezentral organisierten Portallandschaft (Portalverbund Online-Gateway (PVOG)) und noch keine „echte“ Plattform für Verwaltungsleistungen [@Martini_Bürgerkto] [@Seckelmann_sdg]. Das OZG verknüpft also lediglich die Front-Offices als Schnittstelle zu Bürger:innen und Unternehmen [@Engel_OZG]. Aus dem Reifegradmodell ergibt sich, wann eine Verwaltungsleistung OZG-konform umgesetzt wurde (Stufe 3) [@BMI_Leitf]. Der OZG-Servicestandard erweitert das Reifegradmodell um Qualitätsprinzipien für die nutzerfreundliche Digitalisierung von Verwaltungsleistungen [@BMI_Leitf].

![Abb. Reifegradmodell](https://leitfaden.ozg-umsetzung.de/download/attachments/4621600/2.2%20OZG-Reifegradmodell?version=1&modificationDate=1611136705791&api=v2)

Für weitere Informationen siehe [OZG.de](https://www.onlinezugangsgesetz.de/Webs/OZG/DE/grundlagen/info-ozg/info-reifegradmodell/info-reifegradmodell-node.html)

### OZG-Servicestandards

Die OZG-Servicestandards [@OZGSerivestandardUebersicht] definieren Qualitätsrichtlininen [@OZGSerivestandardPoster] für die OZG-Umsetzung. Das oberste Ziel ist die Nutzerorientierung [@OZGSerivestandardPoster]. Dazu zählen die Erhebung von Nutzeranfoderungen, die Intuitivität und Barrierefreiheit der Anwendungen und Sicherheitsvorkehrungen beim Datenaustausch. Weiterhin gibt es Standards zum Vorgehen bei der Digitalisierung, Offenheit des Produkts, dem technischen Betrieb sowie dem Wirkungscontrolling. Zur Offenheit zählen die Verwendung von offenen Standards, Veröffentlichung des Quellcodes als Open-Source und die Wiederverwendungen von einzelnen Bestandteilen. Bei dem technischen Betrieb werden die Zuverlässigkeit, die Ausfallsicherheit und die IT-Sicherheit sowie die Interoperabilität der Komponenten definiert. Schlussendlich gehören zum Wirkungscontrolling die Evaluation der Nutzerzufriedenheit und Nutzungsintensität. 
Diese Servicestandards richten sich an alle Beteiligten vo Bund, Ländern und Kommunen bei der Entwicklung und Optimierung digitaler Verwaltungsleistungen. Hierfür gibt es ein [Selsbtaudit](https://servicestandard.ozg-umsetzung.de/index.php/62918?lang=de), in dem die Entwickler:innen ihr Produkt auf die Einhaltung der Standards testen können. Weitere Tipps und Richtlinien für die Gestaltung und Umsetzung digitaler Verwaltungsleistungen stehen im [Servicehandbuch](https://servicehandbuch.de/). 

## OZG als Projekt
Das OZG-Projekt besteht aus drei zentralen Umsetzungsaktivitäten, die voneinander abhängig sind: der Portalverbund, das Nutzerkonto und die Digitalisierungsprogramme (DP) Bund und Föderal. Der Portalverbund und das Nutzerkonto werden vom IT-Planungsrat koordiniert [@ITPR_UmsetzungOZG]. Die Digitalisierungsprogramme liegen in der Verantwortung des Bundes (DP Bund) bzw. dem Bundesministerium des Innern und für Heimat und der Föderalen IT-Kooperation (FITKO) (DP Föderal) [@ITPR_UmsetzungOZG].

### Digitalisierungsprogramm Bund

IM Digitalisierungsprogramm Bund sind alle Leistungen enthalten die in den Aufgabenbereich des Bundes fallen. Hierbei handelt es sich um 115  Leistungen des Typs 1. Die jeweiligen  Ressorts arbeiten gemeinsam mit BMI an Umsetzung. Die Projektkoordination erfolgt durch das BMI, als Zentraler Ansprechpartner für fachliche Untrstützung ud Verwaltung zentraler Hausmittel zur Umsetzung [@digprog]. 

![](/assets/Rahmen/ozg-strukturen-bund.jpg)

### Digitalisierungsprogramm Föderal

Zu dem Digitalisierungsprogramm Föderal gehören die restlichen 460 Leistungsbündel. Hierbei teilen sich Bund, Länder und Kommunen die arbeit. Die Aufteilung erfolgt nach dem *EfA-Prinzip*, Leistungen mit hoher priorität werden dabei in *Digitalisierungslabor* umgesetzt- für das Programmmanagement sind dabei BMI und FITKO zuständig [@digprogfoed]. 

![](/assets/Rahmen/ozg-strukturen-foederal.jpg)

## OZG-Prämissen: Kooperatives Vorgehen, Nutzerorientierung, EfA-Prinzip
Nach einem Abschichtungsprozess wurden aus dem Leistungskatalog der öffentlichen Verwaltung (LeiKa) 575 Leistungsbündel – zusammengefasst nach Lebens- und Geschäftslagen – identifiziert. Diese Leistungen wurden 14 Themenfeldern zugeordnet, die in einem kooperativen Vorgehen vom zuständigem Bundesressort und dem jeweiligen Land bearbeitet werden [@ozglst]. Die darin enthaltene geteilte Zuständigkeit lässt sich anhand folgender Typisierung nachvollziehen:

* 115 Typ 1 - Leistungen in alleiniger Verantwortung des Bundes
* 370 Typ 2/3 - Leistungen gesetzlich durch Bund geregelt, aber Vollzug durch Länder
* 90 Typ 4/5 - Leistungen mit Regelung und Vollzug durch Länder und Kommunen

welche unabhänig voneinande bearbeitet werden können [@digprogitpl]. 

![](/assets/Rahmen/ozg-leistungen.png)
![](/assets/Rahmen/uebersicht-themenfelder.png)

Die Online-Dienste werden gemeinsam mit User:innen nach dem Primat der Nutzerorientierung in Digitalisierungslaboren entwickelt. Um eine nutzenorientierte, flächendeckende und ressourcensparende Umsetzung des OZG-Programms voranzutreiben, hat man sich auf das Modell „Einer für Alle / Viele“ (EfA) verständigt. Das bedeutet, dass ein Land oder eine Allianz aus mehreren Ländern eine Leistung zentral entwickelt und betreibt. Dieses umsetzende Land stellt die Leistung interessierten Ländern zur Nachnutzung zur Verfügung, wobei sich die anschließenden Länder die Kosten für Betrieb und Weiterentwicklung teilen [@BMI_Wegweiser]. Die Nachnutzung auf Basis von EfA wurde zunächst nur für einen Austausch von Ergebnistypen zwischen Ländern konzipiert mit der Maßgabe, den Kommunen die Leistung entgeltfrei zur Verfügung zu stellen [@BMI_Wegweiser] – denn damit handelt es sich nicht um einen ausschreibungspflichtigen Auftrag im Sinne des Vergaberechts. Sofern eine entgeltliche Weitergabe angestrebt wird, müssen die Anforderungen des Vergaberechts beachtet werden. Aktuell gibt es drei Nachnutzungsmodelle (FIT-Store (https://www.fitko.de/fit-store), Kommunalvertreter-Modell / NRW-Modell (https://www.d-nrw.de/kommunalvertreter) und Marktplatz für EfA-Leistungen / govdigital-Modell (https://www.govdigital.de/marktplatz)), wobei die ersten beiden im Marktplatz für EfA-Leistungen aufgehen werden. Bei den Modellen handelt es sich um ergänzende Ansätze, die ihre rechtliche Hülle nutzen, eine umfassende (vergabefreie) Konzernstruktur der öffentlichen Verwaltung zu organisieren [@Schulz_fit].

## Herausforderungen und Bewertung
Der Großteil der umzusetzenden OZG-Leistungen liegt in der Vollzugskompetenz der Kommunen, weshalb Fragen zur Verfassungskonformität bei Regelung durch ein Bundesgesetz, Einbindung und Finanzierung zu beantworten sind. 
Inwiefern die Kommunen überhaupt aus dem OZG verpflichtet werden können und, ob ggf. ein Anspruch auf Kostenerstattung aus dem Konnexitätsgebot besteht, ist umstritten. Das Konnexitätsgebot („Wer bestellt, bezahlt.“) soll die Kommunen vor finanzieller Überforderung schützen, indem es die Landesebene zu Ausgleichslasten bei der Übertragung neuer Sachaufgaben zwingt [@Martini_Bürgerkto]. Die umfassende Regelung der Verwaltungsdigitalisierung durch ein Bundesgesetz ist aus verfassungsrechtlicher Sicht also nicht unproblematisch, obwohl sie rechtpolitisch richtig und notwendig ist [@Martini_Bürgerkto]. 
Die Einbindung der Kommunen erfolgt im wesentlich komplexeren Digitalisierungsprogramm Föderal u.a. durch die Entsendung von Fachexperten und Beteiligung der kommunalen Spitzenverbände. Ansonsten besteht die Möglichkeit der freiwilligen Mitwirkung. Allerdings bestehen erhebliche Zweifel, ob diese Beteiligung gemessen an der Anzahl der Kommunen (ca. 14.000) und der auf sie entfallenden OZG-Leistungen ausreicht [@BWLöffentlicheVerwaltung]. Um diesem Umstand Rechnung zu tragen, haben einige Bundesländer zusätzlich regionale Kooperationen mit ihren Kommunen institutionalisiert (z.B. Portalverbund NRW oder IT-Verbund Schleswig-Holstein) [@Proll_OZG].
Die Finanzierung von Online-Diensten, die nach dem EfA-Prinzip entwickelt wurden, ist bis Ende 2022 über das Konjunkturprogramm „Corona-Folgen bekämpfen, Wohlstand sichern, Zukunftsfähigkeit stärken“ möglich [@BMI_OZG].
Derzeit laufen Abstimmungen zu den Finanzierungsmodalitäten ab 2023 [@ITPR_efa].  Ohne ein nachhaltiges Finanzierungsmodell besteht die Gefahr, Synergien zwischen den föderalen Ebenen wieder zu verlieren, die durch das kooperative Vorgehen im OZG-Programm entstanden sind.


