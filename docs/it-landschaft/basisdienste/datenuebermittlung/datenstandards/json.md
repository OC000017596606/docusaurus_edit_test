---
sidebar_position: 4
description: Kurze Beschreibung des bekannten Datenformats
---

# JSON
JSON steht für Javascript Object Notation und ist ein leichtgewichtiges Datenformat. Es hat eine gewisse Berühmtheit in der Datenübertragung gewonnen. Mit JSON kann man schnell und leicht beliebige Datenstrukturen darstellen und übertragen. Im Vergleich zu anderen Datenformaten sticht es durch seine simple, kompakte Art heraus. Ebenso ist es menschenlesbar, wodurch man schnell einen Überblick über die übertragenen Daten erhält.  
Die Darstellung basiert auf Text in einer speziellen Form. Das Format besteht aus:

* Zahlen 
* String
* boolsche Werte
* Null
* Array
* Objekte

Wie der Name schon sagt, wird ein Objekt übertragen. Das einfachste Objekt ist: `{ }` - das leere Objekt. Es enthält keine Daten. Fügt man Daten hinzu, macht man das über die Form: `"<Bezeichner>":<Daten>`. `<Daten>` können hierbei alles oben aufgezählte sein. Zahlen werden ohne Anführungsstriche und mit `.` als Trennzeichen für Floatingpoint-Zahlen ausgedrückt. Strings werden in Anführungszeichen geschrieben. Boolsche Werte verwenden die Worte `True` und `False` ohne Anführungszeichen. `Null` steht für einen nicht initialisierten Datentyp. Arrays werden mit `[]` gekennzeichnet. In ihnen dürfen `<Daten>` definiert werden. Und für Objekte werden, wie bereits erwähnt, `{}` genutzt. In diese dürfen Daten in der Form: `"<Bezeichner>":<Daten>` eingefügt werden. Für weitere Informationen hat [Oracle](https://www.oracle.com/de/database/what-is-json/) folgende Seite zur Verfügung gestellt. [@json]

# Quellen
