---
sidebar_position: 1
description: Definition des Plattform-Ansatzes
---

# Allgemeines

Ein möglicher Ansatz, um diese bundesweite IT-Infrastruktur im Mehrebenensystem zu schaffen, wird zurzeit als Plattform-Ansatz verfolgt. O'Reilly (2011) beschreibt dieses Modell als Government as a Plattform.

## Definition: Plattform

Plattformen sind soziotechnische Systeme, die zunächst eine offene Koordination und Kooperation von dezentralen Akteuren ermöglichen, in diesem Fall für die Erarbeitung gemeinsamer digitaler Infrastrukturen [@OReilly_gov]. Auf Basis von geltenden Grundregeln und gemeinsamen Datenschnittstellen (bzw. API Gateways) können dann verschiedene Anwendungen dezentral erstellt werden. Ein analoges Beispiel für Plattformen aus der Privatwirtschaft ist der Apple App Store, der als offenes System mit bestimmten Grundregeln und einer gemeinsamen Infrastruktur, einem API-Gateway, als gemeinsame Datensammelstelle fungiert. Über die Plattform lassen sich dann verschiedene Datenbanken und Anwendungen verknüpfen (siehe dazu Abbildung).

![Offenes System](/assets/Rahmen/Offenes_System__portal1_.png)

## Plattformsystem der bundesdeutschen IT-Landschaft

Im Kontext des [Onlinezugangsgesetzes](docs/grundlagen-und-rahmen/ozg) wird das Zieldesign Plattformsystem anhand eines Vier-Schichten-Modells dargestellt [@BMI_efa]. Darin werden die verschiedenen Architekturschichten des plattformbasierten, zentralen IT-Systems und deren Zusammenwirken veranschaulicht.

![Architekturschichten](/assets/Rahmen/4_schichten__Portal_2_.png)

Über die erste Schicht "Portale" ist der digitale Zugang zu Verwaltungsleistungen und -anliegen geregelt, wie es derzeit über den Portalverbund verfolgt wird. In der zweiten Schicht befinden sich die Basis- und Onlinedienste, die verschiedenen Komponenten und Funktionalitäten, welche mit der untersten Schicht, der Datenhaltung und -Verarbeitung interagieren. Beispiele für die verschiedenen Dienste sind u.a. [Nutzerkonten](docs/it-landschaft/basisdienste/identitaet/nutzerkonto-bund) und [Bezahlkomponenten](docs/it-landschaft/basisdienste/zahlungsabwicklung). In dieser Schicht ist auch das föderale Informationsmanagement ([FIM](docs/grundlagen-und-rahmen/fim)) angesiedelt. Die bereits genannte unterste Schicht umfasst die verschiedenen Fachverfahren und Datenregister. Die Wirksamkeit der verschiedenen Schichten kann nur durch die dritte Schicht, die Interoperabilitätsschicht gewährleistet werden. Diese bildet den Kern des Plattformsystems und ermöglicht die Interaktion zwischen den Datenregistern und den verschiedenen Diensten.

## Quellen
