---
sidebar_position: 2
description: Dastellung des idealtypischen dig. Amtsbesuchs
---

# Digitaler Amtsbesuch mit Basisdiensten

![](/assets/IT-Landschaft/digitaler_amtsbesuch_basisdienste.png)

Die obenstehende Grafik zeigt schematisch die Idealvorstellung eines digitalen Amtsbesuchs ohne Bezug zu einer konkreten Leistung. Sie enthält zudem alle Basiskomponenten, die nach Berücksichtigung der gesetzlichen Grundlagen beim Umsetzen einer Onlineleistung zum Einsatz kommen sollen.

### Auffinden der Leistung
Zum Auffinden einer Onlineleistung nutzt der:die Verwaltungskund:in den Zuständigkeitsfinder <N>2</N> eines [Verwaltungsportals](/docs/it-landschaft/portale/bund-und-laender). Durch Angabe von Leistungs- und Ortsindikatoren findet er:sie so die entsprechende Leistungsbeschreibung und den Link zum Serviceportal der zuständigen Behörde.

Alle Zuständigkeitsfinder arbeiten mit dem [XZufi](/docs/it-landschaft/basisdienste/datenuebermittlung/datenstandards/fim-standards) Standard. Darauf aufbauend ermöglicht der [Portalverbund Online-Gateway](/docs/it-landschaft/portale/pvog) (PVOG) <N>1</N>, dass jede Online-Leistung über jedes zum Portalverbund gehörende Verwaltungsportal auffindbar ist, egal in welchem Bundesland der:die Nutzer:in ansässig ist, auf welchem Portal er:sie startet oder um welche Leistung es sich handelt.

### Nutzeridentifikation
Auf dem Serviceportal der zuständigen Behörde angekommen muss der:die Verwaltungskund:in zunächst seine:ihre Identität nachweisen, bevor eine Onlineleistung in Anspruch genommen werden kann. Hierzu kommt für eine natürliche Personen das [Nutzerkonto Bund](/docs/it-landschaft/basisdienste/identitaet/nutzerkonto-bund) (NKB/BundID) <N>3a</N> zum Einsatz. Dieses identifiziert nicht nur die antragstellende Person, es definiert außerdem unterschiedliche Vertrauensniveaus, welche abhängig von der konkreten Leistung eingefordert und durch die Authentifizierung mittels Nutzername/Passwort, [ELSTER-Zertifikat](/docs/it-landschaft/basisdienste/identitaet/elster) oder [elektronischem Personalausweis](/docs/it-landschaft/basisdienste/identitaet/eid) (eID) erreicht werden können. Die Weitergabe der Authentifizierungsdaten vom Nutzerkonto zum Serviceportal erfolgt im Hintergrund mithilfe von SAML-Metadaten.

Historisch gewachsen verfügen einige Bundesländer über eigene Bürgerkonten. Diese erfüllen im Wesentlichen den gleichen Zweck wie das NKB und werden im Rahmen des Projektes "Föderatives Identitätsmanagement interoperabler Nutzerkonten" (FINK) <N>4</N> so umgestaltet, dass sie gleichermaßen benutzt werden können.

Für Unternehmenskunden bzw. juristische Personen steht das bundeseinheitliche [Unternehmenskonto](/docs/it-landschaft/basisdienste/identitaet/unternehmenskonto) <N>5a</N> bereit. Hier basiert die Authentifizierung ausschließlich auf dem ELSTER-Zertifikat.

### Erfassung der Antragsdaten
Nach der erfolgreichen Authentifizierung der antragstellenden Person erfolgt die Erfassung der für die Bearbeitung des Antrags erforderlichen Daten und Nachweise unter Beachtung aller Richtlinien zu Barrierefreiheit, Datenschutz, -sicherheit und -sparsamkeit. Hierfür bietet das Serviceportal Formularfelder zur Eingabe an, welche die Erhebung der Daten in wohldefinierten, [FIM-konformen Datentypen](/docs/grundlagen-und-rahmen/fim.md) gewährleisten. Eventuell einzureichende Nachweise können als PDF oder in anderen sinnvollen Formaten hochgeladen werden.

Durch Verwendung des bundeseinheitlichen Nutzer- <N>3a</N> bzw. Unternehmenskontos <N>5a</N> besteht als Alternative zur händischen Eingabe, ganz im Sinne des [Once-Only-Prinzips](/docs/grundlagen-und-rahmen/ozg), auch die Möglichkeit, Daten und Nachweise komplett oder in Teilen aus Bestandsdaten zu beziehen. Die Zustimmung des:der Antragsteller:in vorausgesetzt, können so [Datenquellen](/docs/it-landschaft/datenhaltung), z.B. Register und andere interoperable Archivsysteme zu diesem Zwecke genutzt werden, sofern die entsprechenden Daten und Nachweise in der Vergangenheit bereits einmal digital und FIM-konform erfasst und abgelegt wurden. Der:die Nutzer:in kann im Anschluss über das ins Nutzerkonto integrierte Datenschutzcockpit <N>13</N> genau nachvollziehen welche Daten wann, wofür und an wen freigegeben wurden.

Um den [Anforderungen der SDG-VO](https://www.onlinezugangsgesetz.de/Webs/OZG/DE/grundlagen/info-sdg/sdg-anforderungen/sdg-anforderungen-node.html) gerecht zu werden, muss der:die Nutzer:in unmittelbar nach Inanspruchnahme der Onlineleistung die Möglichkeit haben, eine anonyme Bewertung des Serviceportals abzugeben. <N>7</N> Außerdem ist auch die Erhebung von Nutzungsstatistiken im nationalen <N>6a</N> sowie im europäischen Kontext <N>6b</N> erwünscht. Weitere Informationen zu diesen Themen finden sich [hier](/docs/it-landschaft/basisdienste/nutzeranalyse).

### Zahlungsabwicklung
Für zahlreiche Verwaltungsleistungen ist von Kund:innenseite ein Entgelt zu entrichten. Um diese Bezahlvorgänge elektronisch abwickeln zu können, muss sowohl seitens der Serviceportale <N>8a</N> als auch seitens der Kassensysteme bzw. [Fachverfahren](/docs/it-landschaft/fachapplikationen) der empfangenden Behörden <N>8b</N> eine [ePayment-Komponente](/docs/it-landschaft/basisdienste/zahlungsabwicklung) eingebunden werden.

Ein Beispiel für eine solche Komponente ist die Zahlungsplattform ePayBL. Sie wird entwickelt von einer Gemeinschaft aus ITZBund und Vertretern aus zehn Bundesländern und bietet die Möglichkeit gängige Zahlverfahren wie PayPal, Kreditkarte oder SEPA-Lastschrift zu nutzen. Der Einsatz anderer Zahlungsplattformen ist natürlich auch denkbar.

### Transport der Antragsdaten
Nach der Erfassung der Antragsdaten und Nachweise (und ggf. Zahlungsabwicklung) müssen diese nun ihren Weg in das entsprechende Fachverfahren finden. Hierzu werden sie zunächst in ein [XÖV](/docs/it-landschaft/basisdienste/datenuebermittlung/datenstandards/xoev)-konformes Containerformat (bspw. [XFall](/docs/it-landschaft/basisdienste/datenuebermittlung/datenstandards/xfall)) verpackt und anschließend mithilfe eines geeigneten Transportsystems verschlüsselt und an den Zielort übertragen.  
Eine sehr etablierte Übertragungsmöglichkeit ist die Kombination aus [OSCI/XTA und DVDV](/docs/it-landschaft/basisdienste/datenuebermittlung/transportsysteme/osci-xta). <N>9</N> OSCI/XTA spezifiziert hierbei einen Übertragungsstandard, welcher die sichere Kommunikation in unsicheren Netzen (Internet) basierend auf einer öffentlichen Schlüsselinfrastruktur regelt. Es besteht, stark vereinfacht gesprochen, aus einem Metadatensatz, der wie ein Umschlag um die Nutzdaten gelegt wird. Zur technischen Adressierung und Authentisierung wird das DVDV genutzt, welches Behörden in Kategorien einteilt und auf Basis dieser Berechtigungen verteilt.  
Als Alternative dazu kann auch [FIT-Connect](/docs/it-landschaft/basisdienste/datenuebermittlung/transportsysteme/fitconnect) <N>10</N>, eine Entwicklung der FITKO, verwendet werden. Dieses basiert auf der Kommunikation mittels RESTful APIs, bietet eine Ende-zu-Ende-Verschlüsselung und nutzt die PVOG-Infrastruktur <N>1</N> als Routing-Komponente. Es ist technisch sehr gut dokumentiert und somit einfach zu nutzen und bietet darüber hinaus wesentlich mehr Flexibilität beim Einbinden von (nicht behördlichen) Adressaten.  
An dieser Stelle sei außerdem noch die, auf europäischer Ebene sehr relevante, [eDelivery](/docs/it-landschaft/basisdienste/datenuebermittlung/transportsysteme/edelivery)-Variante, basierend auf dem 4-Corner-Modell, als dritte Alternative erwähnt.  
Egal auf welches Transportsystem die Wahl fällt, die entsprechenden Komponenten müssen immer sowohl auf Seiten des Serviceportals als auch auf Seiten des empfangenden Fachverfahrens integriert werden.  

### Rückkanal zum:zur Verwaltungskund:in
Nachdem nun der Antrag bei der Behörde eingegangen ist, erfolgen die Bearbeitung und Bescheidzustellung. An dieser Stelle kommen wieder Nutzer- bzw. Unternehmenskonto ins Spiel, welche ein [Postfach](/docs/it-landschaft/basisdienste/kommunikation/postfach) <N>3b</N> <N>5b</N> und den [Statusmonitor](/docs/it-landschaft/basisdienste/kommunikation/statusmonitor) <N>12</N> integrieren, die als Rückkanal zum:zur Verwaltungskund:in dienen. Alternativ kann die Bescheidzustellung auch über andere Cloudspeicher unabhängig von Nutzerkonten erfolgen.  
Um aus Sicht des:der Kund:in die Identität des:der Absender:in und somit die Echtheit des Bescheides zweifelsfrei sicherstellen zu können, kommen die [Signatur- und Siegeldienste](/docs/it-landschaft/basisdienste/kommunikation/siegel-und-signatur) <N>11</N> der Bundesdruckerei zum Einsatz.
Sollte die digitale Zustellung fehlschlagen oder der Bescheid nach einer [Frist von 10 Tagen](https://www.gesetze-im-internet.de/vwvfg/__41.html) nicht von Kund:innenseite abgerufen werden, erfolgt die Bescheidzustellung per Post.

<!-- JSX-Komponente "N" für grün eingekreiste Zahlen -->
export const N = ({children}) => (
  <span
    style={{
      background: '#03878A',
      mozBorderRadius: '50%',
      webkitBorderRadius: '50%',
      borderRadius: '50%',
      color: '#fff',
      display: 'inline-block',
      lineHeight: '1.8em',
      textAlign: 'center',
      fontSize: 'smaller',
      width: '1.8em',
    }}>
    {children}
  </span>
);
