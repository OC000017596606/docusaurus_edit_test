---
sidebar_position: 6
description: Dienst zum Abruf eigener (sensibler) Basisdaten
---

# Identitätsdatenabruf (IDA)
:::caution Achtung!
Dieser Artikel befindet sich noch in der Entwurfsphase und ist daher möglicherweise inhaltlich noch nicht vollständig und/oder noch nicht ausreichend mit Quellennachweisen belegt.  
Wenn du etwas zu unserem Kompass beitragen möchtest, bearbeite und erweitere die Inhalte. Sei mutig und teile dein Wissen, damit der Kompass der föderalen IT-Architektur weiter lebt.
:::

Das Identitätsdatenabrufverfahren ermöglicht es den Kund:innen der öffentlichen Verwaltung, ihre eigene Identitäsnummer sowie etwaige Basisdaten über die eigene Person beim Bundeszentralamt für Steuern abzurufen und für elektronische Anträge wiederzuverwenden. Es soll eine Schnittstelle für weitere Behörden darstellen und wird vom BVA verantwortet. [@FoederaleITLandschaft]

## Quellen
