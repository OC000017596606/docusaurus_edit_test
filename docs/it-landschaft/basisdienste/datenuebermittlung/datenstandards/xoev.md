---
sidebar_position: 1
description: Ein Rahmenwerk für einheitliche Schnittstellen und Datenstandards
---

# XÖV

XÖV steht für *XML in der öffentlichen Verwaltung*. XÖV wird von der KoSIT entwickelt und betrieben. Dabei wird im XÖV-Rahmenwerk die Grundlage für die Entwicklung von XÖV-Standards festegehalten. XÖV dient der Schaffung einheitlicher Schnittstellen zur Datenübermittlung zwischen Behörden untereinander oder zu deren Kunden. XÖV konzentriert sich also darauf, Datenformate für die Kommunikation mit oder zwischen Behörden und deren Kund:innen zu schaffen. 
Durch das XÖV-Rahmenwerk wird das Prinzip der Wiederverwendung von Komponenten und Methoden genutzt. Dies mindert einerseits den Aufwand der Entwicklung, indem man fortlaufend Komponenten wieder benutzt. Andererseits bietet dieses Schema die Möglichkeit, sich bewährte, qualitativ hochwertige Komponenten für eigene Lösungen zu nutzen. Durch ein gemeinsames Vokabular erhöht es ebenso die Interoperabilität zwischen Programmen. Hierzu werden Komponenten wie Codelisten, also eine annotierte XML-Beschreibung einer Tabelle mitsamt ihrer statischen Inhalte, genutzt. 
Wie der Standard dokumentiert werden soll, wird im [XÖV-Handbuch](https://www.xoev.de/downloads-2316) festgehalten. Durch diese Art der standardisierten Dokumentation wird ein hohe Qualität gewährleistet Dabei unterteilt sich eine Dokumentation in: 

* Einleitung
* UseCases mit Ablaufdiagrammen des Nachrichtenverlaufs
* Baukasten, mit den einzelnen Elementen

Zur Entwicklung der Standards für UML genutzt. Dabei kommt das Tool MagicDraw zum Einsatz. Die daraus resultierenden Artefakte werden von der XÖV zertifiziert und anschließend im [XRepository](https://www.xrepository.de/) veröffentlicht. Im XRepository sind ebenfalls alle Codelisten, die UML Dateien und deren Spezifikationen (Dokumentation) sowie weitere Metadaten veröffentlicht. Weitere Informationen sind [hier]( https://www.xoev.de/) zu finden. [@XoeVHandbuch]

# Quellen
