---
sidebar_position: 1
description: IT-Sicherheit ist ein wichtiger Bestandteil von Gesetzen zum Thema Digitalisierung
---

# Anforderungen aus den Gesetzen
Basierend auf dem [Onlinezugangsgetz (OZG)](/docs/grundlagen-und-rahmen/ozg) [@OZG] sind weitere Verordnungen, Richtlinien und Standards erlassen worden, die das Thema IT-Sicherheit betreffen. In den folgenden Abschnitten werden die zutreffenden Regelungen einzeln näher beleuchtet.

## OZG
Das [OZG](/docs/grundlagen-und-rahmen/ozg) [@OZG] hat einen eigenen Paragraphen für die Gewährleistung der IT-Sicherheit während der Umsetzung:
```
§ 5 IT-Sicherheit
Für die im Portalverbund und für die zur Anbindung an den Portalverbund genutzten IT-Komponenten werden die zur Gewährleistung der IT-Sicherheit erforderlichen Standards durch Rechtsverordnung des Bundesministeriums des Innern, für Bau und Heimat ohne Zustimmung des Bundesrates festgelegt. Die Einhaltung der Standards der IT-Sicherheit ist für alle Stellen verbindlich, die entsprechende IT-Komponenten nutzen. Von den in der Rechtsverordnung getroffenen Regelungen kann durch Landesrecht nicht abgewichen werden. § 4 Absatz 2 gilt entsprechend.
```

Wie aus dem Paragraphen hervorgeht, gibt das BMI die Standards der IT-Sicherheit vor. Ein aktueller Kritikpunkt hierbei ist, dass die entsprechende Rechtsverordnung noch nicht erlassen wurde (Stand 01.09.2020). Außerdem geht aus § 3 "Ziel des Portalverbundes; Nutzerkonten" hervor, dass für die elektronisch verfügbaren Verwaltungsleistungen ein sicheres Verfahren zur Identifizierung und Authentifizierung benötigt wird, die den Anforderungen der Verwaltungsleistung gerecht werden soll. Es soll also ein Nutzerkonto angelegt werden, dass sowohl für natürliche Personen wie auch juristische Personen wie Unternehmen zum Einsatz kommen soll und in Bund und Ländern einheitlich angewendet wird. Weiterhin beschreibt § 8 Absatz 7, dass die eingesetzten Verfahren zum Nachweis der Identität auch im Rahmen des OZG zur Anwendung kommen. Damit ist die eID des Personalausweises gemeint. Diese Regelung ist bis zum 30. Juni 2023 begrenzt. Der vollständige Gesetzestext ist nachzulesen unter: [@OZG]

## SDG
Die [Single-Digital-Gateway Verordnung](/docs/grundlagen-und-rahmen/sdg) [@SDG] beinhaltet auch Aussagen zum Thema Sicherheit.
Es seien an dieser Stelle folgende Auszüge aus der SDG-Präambel aufgeführt, die sich mit IT-Sicherheit befassen:
```
(41) Online-Dienste zuständiger Behörden sind von wesentlicher Bedeutung, um die Qualität und die Sicherheit der Dienste für Bürger sowie Unternehmen zu verbessern. Behörden in Mitgliedstaaten arbeiten zunehmend darauf hin, Daten wiederzuverwenden und dadurch darauf zu verzichten, Bürger sowie Unternehmen mehr als einmal um Vorlage derselben Information zu ersuchen. Die Wiederverwendung von Daten sollte auch für grenzüberschreitende Nutzer vereinfacht werden, um ihnen zusätzlichen Aufwand zu ersparen.

(42) Um den rechtmäßigen grenzüberschreitenden Austausch von Nachweisen und Informationen durch die unionsweite Anwendung des Grundsatzes der einmaligen Erfassung zu ermöglichen, sollten bei der Anwendung dieser Verordnung und des Grundsatzes der einmaligen Erfassung alle anwendbaren Datenschutzvorschriften, einschließlich des Grundsatzes der Datenminimierung, der Richtigkeit, der Speicherbegrenzung, der Integrität und Vertraulichkeit, der Notwendigkeit, der Verhältnismäßigkeit und der Zweckbindung eingehalten werden. Ihre Umsetzung sollte auch vollumfänglich die Grundsätze der eingebauten Sicherheit und des eingebauten Datenschutzes einhalten. und den Grundrechten von Einzelpersonen, einschließlich der Grundrechte, die sich auf Fairness und Transparenz beziehen, Rechnung tragen.

(52) Im Hinblick auf die Gewährleistung eines hohen Sicherheitsniveaus für die grenzüberschreitende Anwendung des Grundsatzes der einmaligen Erfassung durch das technische System sollte die Kommission beim Erlass von Durchführungsrechtsakten zur Festlegung der Spezifikationen für ein solches technisches System den von europäischen und internationalen Normungsorganisationen und -gremien, insbesondere dem Europäischen Komitee für Normung (CEN), dem Europäischen Institut für Telekommunikationsnormen (ETSI), der Internationalen Organisation für Normung (ISO) und der Internationalen Fernmeldeunion (ITU), festgelegten Normen und technischen Spezifikationen sowie den Sicherheitsstandards gemäß Artikel 32 der Verordnung (EU) 2016/679 und Artikel 22 der Verordnung (EU) 2018/1725 umfassend Rechnung tragen.

(54) Die zuständigen Behörden und die Kommission sollten sicherstellen, dass die Informationen, Verfahren und Dienste für die sie verantwortlich sind, den Sicherheitskriterien entsprechen. Die gemäß dieser Verordnung bestellten nationalen Koordinatoren und die Kommission sollten die Einhaltung der Qualitäts- und Sicherheitskriterien auf nationaler Ebene und auf Unionsebene in regelmäßigen Abständen überprüfen und auftretende Probleme angehen. Die nationalen Koordinatoren sollten die Kommission darüber hinaus bei der Überwachung des Funktionierens des technischen Systems, das den grenzüberschreitenden Austausch von Nachweisen ermöglicht, unterstützen. Die vorliegende Verordnung sollte der Kommission verschiedene Mittel an die Hand geben, um einer möglichen Verschlechterung der Qualität der über das Zugangstor angebotenen Dienste entgegenzuwirken; je nachdem, wie schwerwiegend und dauerhaft eine solche Verschlechterung ist, würde auch die Koordinierungsgruppe gegebenenfalls für das Zugangstor mit einbezogen werden. Die Kommission trägt dennoch die Gesamtverantwortung dafür, zu überwachen, dass die Bestimmungen der vorliegenden Verordnung eingehalten werden.

(59) In der vorliegenden Verordnung sollten auch die Zuständigkeiten für die Entwicklung, Verfügbarkeit, Wartung und Sicherheit der IKT-Anwendungen zur Unterstützung des Zugangstors klar zwischen der Kommission und den Mitgliedstaaten aufgeteilt werden. Im Rahmen der Wartungsaufgaben sollten die Kommission und die Mitgliedstaaten regelmäßig prüfen, ob diese IKT-Anwendungen reibungslos funktionieren.

(70) Die Mitgliedstaaten werden darin bestärkt, auf zwischenstaatlicher Ebene ein höheres Maß an Koordinierung, Austausch und Zusammenarbeit zu erreichen, um ihre strategischen, operativen sowie forschungs- und entwicklungsbezogenen Kapazitäten im Bereich der Cybersicherheit zu erhöhen, insbesondere indem sie die in der Richtlinie (EU) 2016/1148 des Europäischen Parlaments und des Rates (NIS) (27) genannte Sicherheit von Netz- und Informationssystemen umsetzen, und um die Sicherheit und Widerstandskraft ihrer öffentlichen Verwaltung und Dienste zu stärken. Die Mitgliedstaaten werden darin bestärkt, die Sicherheit der Transaktionen zu erhöhen und ein ausreichendes Maß an Vertrauen in elektronische Mittel sicherzustellen, indem sie den eIDAS-Rahmen gemäß Verordnung (EU) Nr. 910/2014 und insbesondere angemessene Sicherheitsniveaus umsetzen. Die Mitgliedstaaten können gemäß dem Unionsrecht Maßnahmen zum Schutz der Cybersicherheit und zur Verhütung von Identitätsbetrug oder anderen Formen von Betrug ergreifen.
```
Weiterhin sind folgende Auszüge aus der SDG-Verordnung [@SDG] zu nennen:
```
Art. 1 (3)   Diese Verordnung berührt nicht den Inhalt der Verfahren, die auf der Ebene der Union oder auf nationaler Ebene in irgendeinem der unter diese Verordnung fallenden Bereiche festgelegt sind, oder die Rechte, die im Rahmen dieser Verfahren gewährt werden. Ferner berührt diese Verordnung keine Maßnahmen, die gemäß dem Unionsrecht zur Gewährleistung der Cybersicherheit und zur Verhinderung von missbräuchlichem Verhalten ergriffen werden.

Art. 14 (3)   Das technische System muss insbesondere
a) auf ausdrückliches Ersuchen des Nutzers die Verarbeitung von Anträgen auf Ausstellung von Nachweisen ermöglichen,
b) die Verarbeitung von Anträgen auf Ausstellung von Nachweisen ermöglichen, die zugänglich gemacht oder ausgetauscht werden sollen,
c) die Übermittlung von Nachweisen zwischen den zuständigen Behörden zulassen,
d) die Verarbeitung der Nachweise durch die anfordernde zuständige Behörde zulassen,
e) die Vertraulichkeit und Integrität der Nachweise sicherstellen,
f) dem Nutzer die Möglichkeit bieten, die von der anfordernden zuständigen Behörde zu verwendenden Nachweise vorab einzusehen und zu entscheiden, ob er mit dem Austausch von Nachweisen fortfährt oder nicht,
g) ein angemessenes Maß an Interoperabilität mit anderen einschlägigen Systemen sicherstellen,
h) ein hohes Maß an Sicherheit für die Übermittlung und Verarbeitung von Nachweisen sicherstellen,
i) sicherstellen, dass Nachweise nicht über das für den Austausch von Nachweisen technisch notwendige Maß hinaus und auch dann nur solange verarbeitet werden, wie es der Zweck erfordert.

Art. 14 (11) Die Kommission und jeder Mitgliedstaat sind für die Entwicklung, die Verfügbarkeit, die Wartung, die Kontrolle, die Überwachung und das Sicherheitsmanagement ihrer jeweiligen Teile des technischen Systems verantwortlich.

Art. 30 (1) Die Koordinierungsgruppe für das Zugangstor unterstützt die Ausführung dieser Verordnung. Insbesondere [...] l) erörtert sie die Umsetzung der Grundsätze der eingebauten Sicherheit und des eingebauten Datenschutzes im Rahmen dieser Verordnung,
```
Im Wesentlichen beschreibt die Verordnung den Wunsch, grenzübergreifende Anträge möglich zu machen und dabei auf die eingebaute Sicherheit und den Datenschutz zu achten. Die Mitgliedsstaaten sollen weitgehend unabhängig ihre Schnittstellen entwickeln, wobei die Kommission die Überwachungsfunktion übernimmt.
Der vollständige Gesetzestext ist nachzulesen unter: [@SDG]

## IT-Sicherheitsverordnung Portalverbund
Die IT-Sicherheitsverordnung Portalverbund wurde aufgrund des §5 OZG [@OZG] über die IT-Sicherheit vom BMI erlassen. Sie gliedert sich in technische Richtlinien sowie Vorgaben für Penetrationstests und Webchecks. Weiterhin gelten hierfür die BSI-Standards 200-1, 200-2, 200-3 sowie die Leitlinie für die Informationssicherheit in der öffentlichen Verwaltung. Diese werden in den folgenden Abschnitten aufgeführt.
Außerdem findet das IT-Sicherheitsgesetz 2.0 in diesem Kontext seine Anwendung: [@BSIITSichG]

Eine Rechtsverordnung, die die IT-Sicherheitsstandards der Systeme im Portalverbund sowie die zur Anbindung an den Portalverbund genutzten IT-Komponenten festlegt ist zum aktellen Stand noch nicht erlassen [@FragdenStaatRechtsvITS], sodass die technischen Richtlinien noch nicht rechtsverbindlich sind.

### Technische Richtlinien
Die technischen Richtlinien gliedern sich in:
1. [Vertrauensniveaus und Mechanismen](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/TechnischeRichtlinien/TR03107/TR-03107-1.pdf)
    - [Bewertung von Authentisierungslösungen](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/TechnischeRichtlinien/TR03107/TR-03107-1_Anforderungen.pdf)
    - [Prüfberichtsvorlage](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/TechnischeRichtlinien/TR03107/TR-03107-1_Pruefberichtsvorlage.pdf)
2. [Schriftformersatz mit elektronischem Identitätsnachweis](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/TechnischeRichtlinien/TR03107/TR-03107-2.pdf)
3. [Kryptografische Vorgaben für Projekte der Bundesregierung](https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Standards-und-Zertifizierung/Technische-Richtlinien/TR-nach-Thema-sortiert/tr03116/TR-03116_node.html)
    1. [Telematikinfrastruktur](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/TechnischeRichtlinien/TR03116/BSI-TR-03116.pdf)
    2. [Hoheitliche und eID-Dokumente](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/TechnischeRichtlinien/TR03116/BSI-TR-03116-2.pdf)
    3. [Intelligente Messsysteme](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/TechnischeRichtlinien/TR03116/BSI-TR-03116-3.pdf)
    4. [Kommunikationsverfahren in Anwendungen](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/TechnischeRichtlinien/TR03116/BSI-TR-03116-4.pdf)
        - [Checkliste für Diensteanbieter](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/TechnischeRichtlinien/TR03116/TLS-Checkliste.pdf)
    5. [Anwendungen der Secure Element API](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/TechnischeRichtlinien/TR03116/BSI-TR-03116-5.pdf)
    6. [Kooperative intelligente Verkehrssysteme (C-ITS)](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/TechnischeRichtlinien/TR03116/BSI-TR-03116-6.pdf)
4. [Vertrauensniveaubewertung von Verfahren zur Identifierung von natrlichen Personen](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/TechnischeRichtlinien/TR03147/TR03147.pdf)
    - [Anforderungskatalog zur Prüfung von Identifikationsverfahren](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/TechnischeRichtlinien/TR03147/TR-03147-1_Anforderungen.pdf)
    - [Prüfberichtsvorlage zur Prüfung von Identifikationsverfahren](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/TechnischeRichtlinien/TR03147/TR-03147-1_Pruefberichtsvorlage.pdf)
5. [Servicekonten](https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Standards-und-Zertifizierung/Technische-Richtlinien/TR-nach-Thema-sortiert/tr03160/tr03160_node.html)
    1. [Identifierung und Authentifizierung](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/TechnischeRichtlinien/TR03160/BSI-TR-03160-1.pdf)
    2. [Interoperables Identitätsmanagement für Bürgerkonten](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/TechnischeRichtlinien/TR03160/BSI-TR-03160-2.pdf)

### Vorgaben für Penetrationstests und Webchecks
Das BSI stellt einen Leitfaden [@LeitfadenPentest] mit Checklisten zur Durchführung von Penetrationstests zur Verfügung.
Daraus lassen sich Empfehlungen ableiten. Grundsätzlich soll nach der Umsetzung der IT-Grundschutzmaßnahmen über eine Kurzrevision der IT-Sicherheit die Basisabsicherung nach IT-Grundschutz überprüft werden. Nach eine Webcheck wird der Penetrationstest ausgeführt. Webchecks sind eine Unterkategorie des Penetrationstests bei dem die Funktionalität einer Website überprüft wird. Ein Leitfaden [@LeitfadenITSWebcheck] zur Durchführung eines Webchecks steht zur Verfügung.
Penetrationstests sollten grundsätzlich unter dem 4-Augen-Prinzip mit anschließenden Wiederholungsprüfungen alle zwei bis drei Jahre durchgeführt werden. Es sollen dabei nur bekannte Exploits eingesetzt werden, deren Wirkungsweise schon bekannt ist. Das Schadenspotential einer Schwachstelle bemisst sich in diesem Kontext über den Sicherheitsbedarf der verarbeiteten Daten.
Penetrationstests müssen bestimmte Eigenschaften erfüllen und entsprechend zertifiziert sein. Die Vorgaben sind hier [@BSIPentestkriterien] zu finden.

### Weitere BSI-Standards
Weitere BSI-Standards in Zusammenhang mit der OZG-Umsetzung sind:
 - [BSI-Standard 200-1: Managementsysteme für Informationssicherheit (ISMS)](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Grundschutz/BSI_Standards/standard_200_1.html?nn=128578)
 - [BSI-Standard 200-2: IT-Grundschutz-Methodik](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Grundschutz/BSI_Standards/standard_200_2.html?nn=128640)
 - [BSI-Standard 200-3: Risikomanagement](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Grundschutz/BSI_Standards/standard_200_3.html?nn=128620)
 - [Leitlinie für die Informationssicherheit in der öffentlichen Verwaltung des IT-Planungsrates](https://www.it-planungsrat.de/fileadmin/beschluesse/2019/Beschluss2019-04_TOP12_Anlage_Leitlinie.pdf)
 - [IT-Grundschutz-Profil Basis-Absicherung Kommunalverwaltung](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Grundschutz/Hilfsmittel/Profile/Basis_Absicherung_Kommunalverwaltung.pdf)

### Leitlinie für die Informationssicherheit in der öffentlichen Verwaltung
Die Leitlinien für die Informationssicherheit [@BSIITGrundschutzprofil] untergliedern sich in fünf Handlungsfelder:
1. Informationssicherheitsmanagement
2. Absicherung der IT-Netzinfrastrukturen der öffentlichen Verwaltung
3. Einheitliche Sicherheitsstandards für ebenenübergreifende IT-Verfahren
4. Gemeinsame Abwehr von IT-Angriffen
5. IT-Notfallmanagement

## eIDAS Verordnung über die elektr. Identifizierung und Vertrauensdienste
Die eIDAS Verordnung [@eIDAS][@BSIeIDAS] ist eine Verordnung auf EU-Ebene. Auf nationaler Ebene wird sie in Deutschland durch das eIDAS-Durchführungsgesetz eingebunden.
Durch die Verordnung werden folgende Instanzen geregelt:
    - Vertrauensdienste
    - elektronische Signaturen
    - elektronische Siegel
    - elektronische Zeitstempel
    - Dienste für Zustellung elektronischer Einschreiben
    - Zertifikate für die Website-Authentifizierung
Die eIDAS Verordnung in Text-Form ist hier zu finden: [@eIDAS].

Ein Projekt in Verbindung mit EIDAS ist e-SENS, http://www.esens.eu/. In diesem existiert ein Verbund aus öffentlichen und privaten Akteuren, um die öffentlichen Dienste grenzübergreifend zu ermöglichen. Dieses Projekt wurde inzwischen eingestellt.
Weitere Informationen zu den eIDAS Nodes und deren technische Funktionsweise sind im Kapitel [eIDAS Nodes](/docs/it-landschaft/basisdienste/identitaet/eidas) zu finden.

**Anmerkung**: Es soll eine FAQ-Seite des BMI zu eIDAS geben, zum Zeitpunkt der Erstellung dieses Textes ist diese Seite allerdings nicht erreichbar: https://www.bmi.bund.de/DE/Verwaltung/eIDAS_Verordnung_EU/eIDAS_verordnung_haeufige_fragen/eIDAS_verordnung_haeufige_fragen_node.html


## Quellen
