---
sidebar_position: 4
sidebar_label: DVDV
description: Das DVDV gewährleistet die Identifizierbarkeit von Kommunikationspartner:innen und einen sicher verschlüsselten Datenaustausch
---

# Deutsches Verwaltungsdiensteverzeichnis (DVDV)

## Allgemeines

Hauptziel des deutschen Verwaltungsdiensteverzeichnisses ([DVDV](https://www.fitko.de/produktmanagement/dvdv)) ist die Adressierbarkeit von E-Government-Diensten und die eindeutige Authentifizierung der Kommunikationspartner sowie die vertrauenswürdige Behandlung der Daten. In modernen E-Government-Verfahren werden personenbezogene Daten zahlreicher Fachverfahren (Applikationen) zwischen unterschiedlichen Verwaltungsträgern in Bund, Ländern und Kommunen ausgetauscht. Die zweifelsfreie Identifizierung von Absender:innen und Empfänger:innen, gerade im automatisierten Datenverkehr, und der sicher verschlüsselte elektronische Datenaustausch werden beim DVDV gewährleistet. [@ItzBundDvdvVerfahrenbeschreibung] [@ItzBundDvdvDienstverzeichnis]

## Architektur

Das DVDV besteht aus verschiedenen Komponenten, welche in diesem Kapitel kurz vorgestellt werden. Die Informationen sowie die nachfolgende Übersicht stammen aus den Quellen [@ItzBundDvdvVerfahrenbeschreibung] [@ItzBundDvdvDienstverzeichnis].

![](/assets/IT-Landschaft/architektur_dvdv.png)

### DVDV-Bundesmaster
Der Bundesmaster beinhaltet alle Daten und stellt das zentrale Referenzsystem dar. Änderungen an den Daten können nur an dem Datenbestand des Bundesmasters vorgenommen werden. Um die Sicherheit dieses Systems zu gewährleisten, haben nur die DVDV-Pflegeclients (lesend und schreibend) und die DVDV-Landesserver (passiv) Zugriff. Der Bundesmaster ist nur an das DOI-Netz - nicht ans Internet angeschlossen. Er wird durch das ITZBund bereitgestellt.

### DVDV-Landesserver 
Die dezentralen Landesserver werden vom Land zur Verfügung gestellt und durch einen Replikationsmechanismus vom Bundesmaster über das DOI-Netz mit aktuellen Daten versorgt. Diese haben die Aufgabe, sich die Anfragelast der einzelnen Datenabrufe zu teilen und die Daten (nur lesend) des DVDVs den Nutzer:innen (DVDV-Clients) über das Internet zur Verfügung zu stellen. 

### DVDV Pflegeclients 
Die Pflegeclients sind die einzigen Systeme, die aktiv auf den zentralen Bundesmaster zugreifen und Daten ändern dürfen. Die Kommunikation erfolgt ausschließlich über das DOI-Netz.

### DVDV-Clients
Die Nutzer des DVDVs sind in den meisten Fällen Fachverfahren, welche mittels der Clients auf die Landesserver zugreifen, um Daten abzurufen.

### Clearingstellen
Grundsätzlich sollen diese die Behörden, die das DVDV nutzen wollen, bei der technischen Kommunikation mit DVDV unterstützen (z. B. die sichere Kommunikation der Behörde mit dem zugeordneten DVDV-Landesserver).

### Intermediär 
Der Intermediär ist eine technisch erforderliche Komponente für die [OSCI](https://www.itzbund.de/DE/itloesungen/standardloesungen/osci/osci.html)-konforme Übermittlung der Daten und stellt eine vertrauenswürdige Übermittlungsinstanz dar, welche eingehende Nachrichten prüft und weiterleitet. Dabei kommen Zertifikate zur Authentifizierung und Verschlüsselung zum Einsatz, die der Intermediär verifiziert. Grundsätzlich werden Nachrichten eines Senders im Rahmen des DVDV immer über einen Intermediär dem Empfänger zur Verfügung gestellt.

### DOI-CA
Die DOI-CA ist ein Trust Center, das durch die Deutsche Telekom AG (TeleSec 7) betrieben wird. Sie stellt die Zertifikate für alle DVDV-Kommunikationspartner aus und verwaltet die zugehörigen Zertifikatsinformationen wie die Gültigkeit, um Zertifikate verifizieren zu können. 

### DOI-Netz
Das DOI-Netz ist das Netzwerk der europäischen Verwaltung.

## Sicherheit

### Sicherheitspolicies
Durch die dezentrale Infrastruktur hat das DVDV-System eine hohe Ausfallsicherheit. [@ItzBundDvdvVerfahrenbeschreibung] Fällt ein Landesserver aus, kann der Client einen anderen anfragen, da diese die gleichen Daten zur Verfügung stellen.

### Implementierung
Der Quellcode und die Dokumentation der Server sind nicht öffentlich verfügbar und können somit nicht analysiert werden. Für die Erleichterung der Anbindung an das DVDV werden [Development Kits](https://www.itzbund.de/DE/itloesungen/standardloesungen/dvdv/downloads/downloads.html?nn=178712#title3134781) zur Verfügung gestellt, welche Beispielcode für Abfragen beinhalten.

### Autorisierung und Authentifizierung
Alle an DVDV teilnehmenden Behörden benötigen zu ihrer Authentifizierung Zertifikate. Darin sind Schlüssel für ihre eindeutige Identifizierung und die Verschlüsselung der Nachrichten enthalten. Das DVDV selbst stellt keine Zertifikate aus, verwendet sie aber. Diese müssen regelmäßig – in der Regel alle zwei Jahre – erneuert werden. Ausgestellt werden die Zertifikate vom Technologiedienstleister TSystems. Er nutzt dafür eine Public Key Infrastructure (PKI). Das DVDV überprüft die Gültigkeit der Zertifikate anhand der Sperrlisten, die von der Zertifizierungsstelle oder certificate authority (CA) der NdB zur Verfügung gestellt werden. [@ItzBundDvdvDienstverzeichnis]
Für eine einheitliche Authentifizierung über alle DVDV-Server wird beim Bundesmaster-Betreiber ein zentrales „Identity and Access Management“ (DVDV-IAM) in Form eines Keycloak-Servers aufgesetzt. Bei der Anfrage auf den DVDV-Server wird immer zunächst auf das DVDV-IAM umgeleitet werden, um dort ein Access-Token zu beziehen gemäß OAuth2. [@ItzBundDvdvVerfahrenbeschreibung]

### Updates
Es erfolgt ein Update pro Quartal.

### Datenhaltung
Für die Datenspeicherung wird OpenLDAP und eine MySQL Datenbank verwendet. Es werden unter anderem Identifizierungsdaten, wie Protokoll- und Adressierungsinformationen für Sender und Empfänger gespeichert. [@ItzBundDvdvVerfahrenbeschreibung]

### Datenschutz
Es wurde keine Datenschutzerklärung gefunden.

### Kryptografie
Die Verschlüsselung der Kommunikation erfolgt auf Basis des Übermittlungsprotokols OSCI. Es werden somit Integrität, Authentizität, Vertraulichkeit und Nachvollziehbarkeit bei der Übermittlung von Nachrichten gewährleistet. Weiterhin ermöglicht es den Behörden sensible Geschäftsvorfälle rechtsverbindlich abgesichert über das Internet anzubieten. Die vertrauliche Ende-zu-Ende Datenübermittlung erfolgt mittels Signatur (fortgeschrittene elektronische Signatur nach § 2 Nr. 2 SigG) und Verschlüsselung. Bei der Verschlüsselung wird das Prinzip des doppelten Umschlags verwendet, wobei der Inhalt der Nachricht vom Empfänger und die Kommunikationsdaten von der Vermittlungsstelle (Intermediär) verschlüsselt werden. Governikus stellt für jeden DVDV Server einen eigenen Intermediär, der ausgehende Nachrichten verschlüsselt und bei eingehenden Nachrichten die Authentifikation übernimmt. Im Rahmen der OSCI-Kommunikation werden für diesen Zweck Zertifikate der DOI CA (PKI) eingesetzt. Diese müssen auf Basis von Chipkarten zum Einsatz kommen. [@ItzBundDvdvVerfahrenbeschreibung] Die Validierung der Zertifikate erfolgt durch das Trust Center. Weitereführende Informationen zu dem Zertifikatsmanagement können in Quelle [@ItzBundDvdvZertifikate] eingesehen werden.

### Responsible Disclosure
Es sind keine Informationen zu Responsible Disclosure oder dem IT-Sicherheitsbeauftragten vorhanden. 

### Ausfallsicherheit 
Die Ausfallsicherheit ist durch die redundanten Landesserver gewährleistet. Jeder DVDV-Landesserver kann jeden anderen bei Ausfall oder Überlastung vertreten. [@ItzBundDvdvVerfahrenbeschreibung]

## Beziehungen zu anderen Basisdiensten
FIT-Connect hat im Zusammenspiel mit dem DVDV zwei unterschiedliche Anwendungsfälle:
1. OSCI-Kommunikation zwischen Zustelldienst und Fachanwendung: Hierzu kann die bestehende DVDV-Infrastruktur unverändert genutzt werden.
2. Hinterlegung von technischen Parametern für die XFall/REST-Kommunikation zwischen Zustelldienst und Fachanwendung: Hierzu wurde für die Projektphase von FIT-Connect ein DVDV-Microservice entwickelt, der die Erprobung der Anbindung der FIT-Connect-Antragsinfrastruktur (Zustelldienst + Routingdienst) an das DVDV ermöglicht. [@FitkoDvdv]

## Quellen
